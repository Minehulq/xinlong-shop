export const columns = [
<#list table.fields as field>
  {
    title: '${field.comment}',
    key: '${field.propertyName}',
    width: 100,
  },
</#list>
];
