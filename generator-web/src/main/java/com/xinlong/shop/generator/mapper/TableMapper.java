package com.xinlong.shop.generator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xinlong.shop.generator.entity.TableInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 22:39 2022/6/20
 */
@Mapper
public interface TableMapper extends BaseMapper<TableInfo> {

    List<TableInfo> findTables(@Param("tableName") String tableName);

}
