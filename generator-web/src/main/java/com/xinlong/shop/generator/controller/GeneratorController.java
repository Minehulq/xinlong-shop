package com.xinlong.shop.generator.controller;

import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.generator.entity.TableInfo;
import com.xinlong.shop.generator.model.GeneratorVO;
import com.xinlong.shop.generator.service.IGeneratorService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 01:56 2022/6/14
 */
@RestController
@RequestMapping("/sys")
public class GeneratorController {

    private final IGeneratorService generatorService;

    public GeneratorController(IGeneratorService generatorService) {
        this.generatorService = generatorService;
    }

    /**
     * 根据表名获取相关表的信息
     * @param tableName
     * @return
     */
    @GetMapping("/tables")
    public R tables(String tableName){
        List<TableInfo> list = generatorService.findTablesByTableName(tableName);
        return R.success(list);
    }

    /**
     * 根据表名获取字段信息合集
     * @param tableNames
     * @return
     */
    @GetMapping("/fields")
    public R fieldInfos(String[] tableNames) {
        if(tableNames == null || tableNames.length < 1) {
            return R.error("至少包含一个表名");
        }
        Map<String,List> list = generatorService.findFieldsByTableName(tableNames);
        return R.success(list);
    }

    /**
     * 基础代码生成
     * @param generatorVO 参数vo
     */
    @PostMapping("/base-gen")
    public R generatorBase(@RequestBody @Validated GeneratorVO generatorVO) {
        try {
            generatorService.execute(generatorVO);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return R.success();

    }

}
