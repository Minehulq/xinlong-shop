package com.xinlong.shop.generator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xinlong.shop.generator.entity.FieldInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 02:29 2022/6/23
 */
@Mapper
public interface FieldMapper extends BaseMapper<FieldInfo> {

    List<FieldInfo> findFields(@Param("tableName") String tableName);

}
