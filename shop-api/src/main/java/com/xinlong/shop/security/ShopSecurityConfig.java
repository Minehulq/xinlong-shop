package com.xinlong.shop.security;


import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Web 安全配置
 * @Author Sylow
 * @Date 2022/5/21
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Order(102)
public class ShopSecurityConfig extends WebSecurityConfigurerAdapter {


    /**
     * 此方法配置的资源路径不会进入 Spring Security 机制进行验证
     */
    @Override
    public void configure(WebSecurity web) {
        web.ignoring()
                .antMatchers(HttpMethod.OPTIONS, "/**")
                .antMatchers("/favicon.ico")
                .antMatchers("/v3/api-docs")
                .antMatchers("/webjars/**")
                .antMatchers("/swagger-resources/**")
                .antMatchers("/buyer/home/**")
                .antMatchers("/buyer/classify/**")
                .antMatchers("/buyer/goods/**")
                .antMatchers("/buyer/captcha/**")
                .antMatchers("/buyer/member/token")
                .antMatchers("/buyer/member/login")
                .antMatchers("/buyer/member/send-code")
                .antMatchers("/buyer/uploader")

                //.antMatchers("/buyer/member/password/send-code")    // 修改密码发送短信和忘记重置密码发短信同一个

                .antMatchers("/buyer/member/register")
                // 微信相关
                .antMatchers("/buyer/wx/mp/**")
                .antMatchers("/buyer/wx/pay/callback")
                .antMatchers("/buyer/wx/pay/get-code-url")
                // 盲盒相关
                .antMatchers("/buyer/blind-box/**")

                // 文章广告首页等
                .antMatchers("/buyer/article/**")
                .antMatchers("/buyer/adv/**")
                .antMatchers("/buyer/home-site/**")
                // 小程序登录
                .antMatchers("/buyer/wx/mini/login")

                .antMatchers("/buyer/logout")
                .antMatchers("/doc.html");
    }

}
