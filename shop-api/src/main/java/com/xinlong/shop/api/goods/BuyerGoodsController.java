package com.xinlong.shop.api.goods;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.core.goods.entity.GoodsQueryParam;
import com.xinlong.shop.core.goods.entity.dto.GoodsDTO;
import com.xinlong.shop.core.goods.service.IGoodsService;
import com.xinlong.shop.framework.common.R;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 10:32 2022/12/7
 */
@RestController
@RequestMapping("/buyer/goods")
public class BuyerGoodsController {
    private final IGoodsService goodsService;

    public BuyerGoodsController(IGoodsService goodsService) {
        this.goodsService = goodsService;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size, GoodsQueryParam goodsQueryParam){

        String sortName = goodsQueryParam.getSortName();
        String sortType = goodsQueryParam.getSortType();

        if (!StrUtil.isEmpty(sortName) && !StrUtil.isEmpty(sortType)) {
            switch (sortName) {
                case "price":
                    goodsQueryParam.setSortName("goods_price");
                    break;
                case "create":
                    goodsQueryParam.setSortName("create_time");
                    break;
                case "eva":
                    // 没有评价 暂时用id代替
                    goodsQueryParam.setSortName("id");
                    break;
                default:
                    goodsQueryParam.setSortName("id");
                    break;
            }
            if ("asc".equals(sortType)) {
                goodsQueryParam.setSortType("asc");
            } else {
                goodsQueryParam.setSortType("desc");
            }
        }

        IPage<GoodsDTO> page = new Page<>(current, size);
        page = this.goodsService.page(page, goodsQueryParam);
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public R goods(@PathVariable("id") Integer goodsId){
        GoodsDTO goods = this.goodsService.findById(goodsId);
        return R.success("操作成功", goods);
    }
}
