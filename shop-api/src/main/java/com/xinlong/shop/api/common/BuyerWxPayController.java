package com.xinlong.shop.api.common;

import cn.hutool.core.util.StrUtil;
import com.xinlong.shop.core.blind.service.IBlindBoxOrderService;
import com.xinlong.shop.core.image.service.IImageOrderService;
import com.xinlong.shop.core.order.service.IOrderService;
import com.xinlong.shop.core.promotion.service.IPutSaleOrderService;
import com.xinlong.shop.core.util.OrderSnTypeEnum;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.framework.weixin.config.WeiXinMpConfig;
import com.xinlong.shop.framework.weixin.service.WeiXinMpService;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 00:36 2023/3/22
 */
@RestController
@RequestMapping("/buyer/wx/pay")
public class BuyerWxPayController {

    private final WeiXinMpConfig weiXinMpConfig;
    private final WeiXinMpService weiXinMpService;
    private final IPutSaleOrderService putSaleOrderService;
    private final IBlindBoxOrderService blindBoxOrderService;
    private final IImageOrderService imageOrderService;
    private final IOrderService orderService;

    public BuyerWxPayController(WeiXinMpConfig weiXinMpConfig, WeiXinMpService weiXinMpService, IPutSaleOrderService putSaleOrderService, IBlindBoxOrderService blindBoxOrderService, IImageOrderService imageOrderService, IOrderService orderService) {
        this.weiXinMpConfig = weiXinMpConfig;
        this.weiXinMpService = weiXinMpService;
        this.putSaleOrderService = putSaleOrderService;
        this.blindBoxOrderService = blindBoxOrderService;
        this.imageOrderService = imageOrderService;
        this.orderService = orderService;
    }

    @RequestMapping(value = "/callback", method = RequestMethod.POST)
    public R callBack(@RequestHeader("Wechatpay-Serial") String wechatpaySerial,
                      @RequestHeader("Wechatpay-Signature") String wechatpaySignature,
                      @RequestHeader("Wechatpay-Timestamp") String wechatpayTimestamp,
                      @RequestHeader("Wechatpay-Nonce") String wechatpayNonce, @RequestBody String requestBody){
        Map<String,String> result = weiXinMpService.callBack(wechatpaySerial, wechatpaySignature, wechatpayTimestamp, wechatpayNonce, requestBody);

        if (result == null) {
            return R.error("没有订单编号");
        }
        String orderSn = result.get("orderSn");
        String payOrderSn = result.get("payOrderSn");
        // 盲盒商品
        if (orderSn.startsWith(String.valueOf(OrderSnTypeEnum.BLIND_BOX.getCode()))) {
            // 盲盒商品回调函数  处理盲盒商品购买成功
            //System.out.println("盲盒订单回调");
            blindBoxOrderService.paySuccess(orderSn, payOrderSn);
        }
        // 普通订单（盲盒商品提货支付运费也在这里）
        if (orderSn.startsWith(String.valueOf(OrderSnTypeEnum.ORDER.getCode()))) {
            orderService.paySuccess(orderSn, payOrderSn);
        }
        // 抢购商品
        if (orderSn.startsWith(String.valueOf(OrderSnTypeEnum.PURCHASE.getCode()))) {

            // 上架结果
            putSaleOrderService.putSaleOrder(orderSn, payOrderSn);
        }
        // 图片订单
        if (orderSn.startsWith(String.valueOf(OrderSnTypeEnum.IMAGE_ORDER.getCode()))) {
            imageOrderService.paySuccess(orderSn, payOrderSn);
        }
            //if ()
        //System.out.println(weiXinNotify.getEventType());
        return R.success();
    }

    @RequestMapping(value = "/get-code-url", method = RequestMethod.GET)
    public R getCodeUrl(String redirectUri) {
        if (StrUtil.isBlank(redirectUri)) {
            return R.error("回调地址必填");
        }

        String appId = weiXinMpConfig.getAppid();
        String url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" +
                    appId +
                    "&redirect_uri=" +
                    redirectUri +
                    "&response_type=code&scope=snsapi_base&state=code#wechat_redirect";

        return R.success("操作成功",url);
    }

    /**
     * 测试用的
     * @param code
     * @return
     */
    @RequestMapping(value = "/openid", method = RequestMethod.GET)
    public R getOpenId(String code){
//        String openId = weiXinMpService.getOpenId(code);
//        String orderId = "t100005";
//        PrepayWithRequestPaymentResponse response = weiXinMpService.pay(orderId, openId);
//        return R.success("操作成功", response);
        return R.success();
    }


}

