//package com.xinlong.shop.api.image;
//
//import com.xinlong.shop.api.common.BuyerBaseController;
//import com.xinlong.shop.core.member.service.IMemberService;
//import com.xinlong.shop.core.site.entity.Adv;
//import com.xinlong.shop.framework.common.R;
//import com.xinlong.shop.framework.security.constant.SecurityConstants;
//import com.xinlong.shop.framework.security.util.TokenUtil;
//import com.xinlong.shop.framework.tencent.FaceCartoonPic;
//import com.xinlong.shop.framework.tencent.ImageToImage;
//import io.swagger.annotations.*;
//import org.springframework.validation.annotation.Validated;
//import org.springframework.web.bind.annotation.RequestHeader;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.validation.constraints.NotNull;
//
///**
// * @Author Sylow
// * @Description 会员端图片处理
// * @Date: Created in 05:56 2023/8/3
// */
//@Validated      // get请求验证参数时需要
//@RestController
//@Api(tags = "买家端图片处理API")
//@RequestMapping("/buyer/image")
//public class BuyerImageController extends BuyerBaseController {
//
//    private final FaceCartoonPic faceCartoonPic;
//    private final ImageToImage imageToImage;
//
//    public BuyerImageController(IMemberService memberService, TokenUtil tokenUtil, FaceCartoonPic faceCartoonPic, ImageToImage imageToImage) {
//        super(memberService, tokenUtil);
//        this.faceCartoonPic = faceCartoonPic;
//        this.imageToImage = imageToImage;
//    }
//
//    @ApiOperation(value = "生成卡通头像")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "url", value = "图片地址", required = true, paramType = "query"),
//    })
//    @ApiResponses(@ApiResponse(code = 200, message = "操作成功", response = Adv.class))
//    @RequestMapping(value = "/cartoon-face", method = RequestMethod.GET, produces = "application/json")
//    public R cartoonFace(@NotNull(message = "图片地址不能为空") String url, @RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){
//        if (getMember(token) == null) {
//            return R.error("请先登录");
//        }
//        String cartoonPic = this.faceCartoonPic.generate(url);
//        return R.success(cartoonPic);
//    }
//
//    @ApiOperation(value = "图生图")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "url", value = "广告位编码", required = true, paramType = "query"),
//    })
//    @ApiResponses(@ApiResponse(code = 200, message = "操作成功", response = Adv.class))
//    @RequestMapping(value = "/img-to-img", method = RequestMethod.GET, produces = "application/json")
//    public R imgToImg(@NotNull(message = "图片地址不能为空") String url, @RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){
//        if (getMember(token) == null) {
//            return R.error("请先登录");
//        }
//        String img = this.imageToImage.generate(url);
//        return R.success("操作成功", img);
//    }
//}
