package com.xinlong.shop.api.promotion;

import cn.hutool.core.date.DateUtil;
import com.xinlong.shop.api.common.BuyerBaseController;
import com.xinlong.shop.core.member.service.IMemberService;
import com.xinlong.shop.core.promotion.entity.RushPurchaseCart;
import com.xinlong.shop.core.promotion.service.IRushPurchaseCartService;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.framework.core.entity.Member;
import com.xinlong.shop.framework.security.constant.SecurityConstants;
import com.xinlong.shop.framework.security.util.TokenUtil;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 抢购购物车 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2023-05-05
 */
@RestController
@RequestMapping("/buyer/promotion/rush-purchase-cart")
public class BuyerRushPurchaseCartController extends BuyerBaseController {

    private final IRushPurchaseCartService rushPurchaseCartService;

    public BuyerRushPurchaseCartController(IMemberService memberService, TokenUtil tokenUtil, IRushPurchaseCartService rushPurchaseCartService) {
        super(memberService, tokenUtil);
        this.rushPurchaseCartService = rushPurchaseCartService;
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public R list(@RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){
//        Page<RushPurchaseCart> page = new Page<>(current, size);
//        page = this.rushPurchaseCartService.page(page);
        Member member = getMember(token);
        Integer memberId = member.getId();
        List<RushPurchaseCart> list = this.rushPurchaseCartService.listByMemberId(memberId);
        return R.success("操作成功", list);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody RushPurchaseCart rushPurchaseCart, @RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){
        Member member = getMember(token);
        Integer memberId = member.getId();

        // 只有正常的购物车特权是没限制的
        if (member.getGradeId().intValue() != 5) {
            int goodsNum = this.rushPurchaseCartService.countByMemberId(memberId);
            if (goodsNum >= 6) {
                return R.error("购物车最多只能添加6件");
            }
        }
        // 购物车价格限制
//        if (rushPurchaseCart.getPrice().compareTo(BigDecimal.valueOf(10000)) != 1) {
//            return R.error("1万以下的商品不能加入购物车");
//        }

        rushPurchaseCart.setMemberId(memberId);
        this.rushPurchaseCartService.add(rushPurchaseCart);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/have", method = RequestMethod.GET)
    public R ishave(Integer rushPurchaseDetailId){
        if (rushPurchaseDetailId == null) {
            return R.error("参数错误");
        }
        boolean result = this.rushPurchaseCartService.isHave(rushPurchaseDetailId);
        return R.success("操作成功", result);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        this.rushPurchaseCartService.delete(id);
        return R.success("删除成功");
    }

    @RequestMapping(value = "/clear", method = RequestMethod.DELETE)
    public R clear(@RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token) {
        Member member = getMember(token);
        Integer memberId = member.getId();
        this.rushPurchaseCartService.deleteByMemberId(memberId);
        return R.success("删除成功");
    }

    @RequestMapping(value = "/buy", method = RequestMethod.POST)
    public R buy(@RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){


        Member member = getMember(token);
        String result = this.rushPurchaseCartService.batchBuy(member);

        return R.success("操作成功", result);
    }

//    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
//    public R update(@Valid @RequestBody RushPurchaseCart rushPurchaseCart, @PathVariable("id") Integer id){
//        this.rushPurchaseCartService.update(rushPurchaseCart, id);
//        return R.success("操作成功",rushPurchaseCart);
//    }

    public static void main(String[] args) {
        long time = 1683734400000L;
        long time1 = 86400000L;

        System.out.println(DateUtil.date(time));
        System.out.println(DateUtil.date(time+time1));
    }

}
