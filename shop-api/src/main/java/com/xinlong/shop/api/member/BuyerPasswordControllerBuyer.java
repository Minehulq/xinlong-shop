package com.xinlong.shop.api.member;

import com.xinlong.shop.api.common.BuyerBaseController;
import com.xinlong.shop.core.member.service.IMemberService;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.framework.core.entity.Member;
import com.xinlong.shop.framework.security.constant.SecurityConstants;
import com.xinlong.shop.framework.security.util.TokenUtil;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Sylow
 * @Description 会员修改密码 重置手机 找回密码等
 * @Date: Created in 00:18 2023/04/18
 */
@RestController
@RequestMapping("/buyer/member/password")
public class BuyerPasswordControllerBuyer extends BuyerBaseController {

    public BuyerPasswordControllerBuyer(IMemberService memberService, TokenUtil tokenUtil) {
        super(memberService,tokenUtil);
    }


    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public R update(String password, String code, @RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token){
        if(password == null || password.length() < 6 || password.length() >20){
            return R.error("密码长度在6-20位");
        }
        if (code == null || code.length() != 6) {
            return R.error("验证码长度不正确");
        }

        Member member = getMember(token);

        boolean result = this.memberService.verifySmsCode(member.getMobile(), "password", code);
        if (!result) {
            return R.error("验证码错误");
        }

        this.memberService.updatePassword(member.getId(), password);
        return R.success("修改成功");
    }

}
