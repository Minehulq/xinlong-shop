package com.xinlong.shop.api.blind;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.core.blind.entity.BlindBox;
import com.xinlong.shop.core.blind.entity.BlindBoxGoods;
import com.xinlong.shop.core.blind.entity.vo.BlindBoxVO;
import com.xinlong.shop.core.blind.mapstruct.BlindBoxStruct;
import com.xinlong.shop.core.blind.service.IBlindBoxGoodsService;
import com.xinlong.shop.core.blind.service.IBlindBoxService;
import com.xinlong.shop.framework.common.R;
import io.swagger.annotations.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author Sylow
 * @Description 用户端盲盒接口
 * @Date: Created in 13:48 2023/6/8
 */
@Validated      // get请求验证参数时需要
@RestController
@Api(tags = "买家盲盒API")
@RequestMapping("/buyer/blind-box")
public class BuyerBlindBoxController {

    private final IBlindBoxService blindBoxService;
    private final IBlindBoxGoodsService blindBoxGoodsService;
    private final BlindBoxStruct blindBoxStruct;

    public BuyerBlindBoxController(IBlindBoxService blindBoxService, IBlindBoxGoodsService blindBoxGoodsService, BlindBoxStruct blindBoxStruct) {
        this.blindBoxService = blindBoxService;
        this.blindBoxGoodsService = blindBoxGoodsService;
        this.blindBoxStruct = blindBoxStruct;
    }


    @ApiOperation(value = "获取盲盒商品分页列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "current", value = "当前页数", required = true, paramType = "query"),
            @ApiImplicitParam(name = "size", value = "每页数量", required = true, paramType = "query"),
    })
    @ApiResponses(@ApiResponse(code = 200, message = "操作成功", response = BlindBox.class))
    @RequestMapping(value = "/page", method = RequestMethod.GET, produces = "application/json")
    public R page(@NotNull(message = "当前页不能为空") Integer current, @NotNull(message = "每页数量不能为空") Integer size){
        Page<BlindBox> page = new Page<>(current, size);
        page = this.blindBoxService.page(page);
        return R.success(page);
    }

    @ApiOperation(value = "获取盲盒商品详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "盲盒商品id", required = true, paramType = "path"),
    })
    @ApiResponses(@ApiResponse(code = 200, message = "操作成功", response = BlindBoxVO.class))
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public R blindBoxDetail(@PathVariable("id") Integer id){
        BlindBox blindBox = this.blindBoxService.getById(id);
        if (blindBox == null) {
            return R.error("盲盒商品不存在");
        } else {
            BlindBoxVO blindBoxVO = blindBoxStruct.toBlindBoxVO(blindBox);
            List<BlindBoxGoods> goodsList = this.blindBoxGoodsService.findByBlindBoxId(id);
            blindBoxVO.setGoodsList(goodsList);
            return R.success(blindBoxVO);
        }
    }
}
