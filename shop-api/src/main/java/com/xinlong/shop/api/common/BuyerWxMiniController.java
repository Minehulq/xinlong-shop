package com.xinlong.shop.api.common;

import com.xinlong.shop.core.member.entity.vo.MemberLoginVO;
import com.xinlong.shop.core.member.service.IMemberService;
import com.xinlong.shop.core.site.entity.Adv;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.framework.core.entity.Member;
import com.xinlong.shop.framework.security.constant.SecurityConstants;
import com.xinlong.shop.framework.security.util.TokenUtil;
import com.xinlong.shop.framework.weixin.service.WeiXinMiniService;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * @Author Sylow
 * @Description 微信小程序登录
 * @Date: Created in 14:13 2023/8/3
 */
@RestController
@RequestMapping("/buyer/wx/mini")
public class BuyerWxMiniController extends BuyerBaseController {

    private final WeiXinMiniService weiXinMiniService;

    public BuyerWxMiniController(IMemberService memberService, TokenUtil tokenUtil, WeiXinMiniService weiXinMiniService) {
        super(memberService, tokenUtil);
        this.weiXinMiniService = weiXinMiniService;
    }


    @ApiOperation(value = "小程序自动登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "微信code", required = true, paramType = "query"),
    })
    @ApiResponses(@ApiResponse(code = 200, message = "操作成功", response = Adv.class))
    @RequestMapping(value = "/login", method = RequestMethod.GET, produces = "application/json")
    public R login(String code, String inviteMemberNo){
        String openId = weiXinMiniService.getOpenId(code);
        if ("".equals(openId)) {
            return R.error("登录失败");
        }
        Member member = this.memberService.findByOpenId(openId);
        if (member == null) {
            // 注册 并且赠送积分
            member = this.memberService.registerByOpenId(openId, inviteMemberNo);
            memberService.addPoints(member.getId(), BigDecimal.valueOf(5), 1,"system",   "新用户注册赠送积分");
        }
        MemberLoginVO memberLoginVO = memberService.login(member.getMemberName(), "", false);
        if (memberLoginVO.getMember().getStatus().intValue() != 0) {
            return R.error("账号已被禁用");
        }

        // 记住我功能后期再开发，现在默认就是记住
        String refreshToken = this.memberService.generateRefreshToken(member.getMemberName(), true);
        memberLoginVO.setRefreshToken(refreshToken);
        return R.success(memberLoginVO);
    }

    @ApiOperation(value = "获取小程序码")
    @ApiResponses(@ApiResponse(code = 200, message = "操作成功"))
    @RequestMapping(value = "/wxa-code", method = RequestMethod.GET, produces = "application/json")
    public R getWxaCode(@RequestHeader(value = SecurityConstants.TOKEN_HEADER, required = true) String token) {
        Member member = getMember(token);
        if (member == null) {
            return R.error("请先登录");
        }
        String scene = "memberNo=" + member.getMemberNo();
        String url = this.weiXinMiniService.getMiniQrCode(scene, null);
        return R.success("操作成功", url);
    }

}
