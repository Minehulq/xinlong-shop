package com.xinlong.shop.api.common;

import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.framework.service.ISysSettingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 16:57 2023/5/31
 */
@RestController
@Api(tags = "系统配置API")
@RequestMapping("/buyer/sys/setting")
public class BuyerSysSettingController {

    private final ISysSettingService sysSettingService;

    public BuyerSysSettingController(ISysSettingService sysSettingService) {
        this.sysSettingService = sysSettingService;
    }

    @ApiOperation(value = "获取系统配置")
    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public R list(){
        Map<String,Object> settings = this.sysSettingService.getSetting();
        return R.success("操作成功", settings);
    }
}
