//package com.xinlong.shop.core;
//
//import com.xinlong.shop.framework.core.entity.SysFile;
//import com.xinlong.shop.framework.service.ISysFileService;
//import com.xinlong.shop.framework.tencent.ImageToImage;
//import com.xinlong.shop.framework.util.FileUtils;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.annotation.Resource;
//import java.io.File;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * @Author Sylow
// * @Description 生成图片测试
// * @Date: Created in 17:37 2023/6/13
// */
//@SpringBootTest
//@RunWith(SpringRunner.class)
//public class GenerateImgTest {
//
//    @Resource
//    private ISysFileService sysFileService;
//
//    @Resource
//    private ImageToImage imageToImage;
//
//    private String filePath = "/Users/Sylow/Downloads/20230809";
//    private String allowedTypes = "jpg,png,jpeg";   // 允许类型
//
//    @Test
//    public void generateTest() throws IOException {
//
//        File dir = new File(filePath);
//        File[] files = dir.listFiles();
//        List<MultipartFile> uploadFiles = new ArrayList<>();
//        // 转换类型
//        for (File file : files) {
//            // 类型筛选 不支持就跳过
//            String fileName = file.getName();
//            String fileType = fileName.substring(fileName.lastIndexOf(".") + 1);
//            fileType = fileType.toLowerCase();
//            if (allowedTypes.indexOf(fileType) == -1) {
//                continue;
//            }
//            MultipartFile multipartFile = FileUtils.getMultipartFile(file);
//            uploadFiles.add(multipartFile);
//        }
//
//        // 上传
//        List<SysFile> sysFiles = new ArrayList<>();
//        for (MultipartFile uploadFile : uploadFiles) {
//            SysFile sysFile = sysFileService.upload(uploadFile, "test");
//            sysFiles.add(sysFile);
//        }
//
//        // 如果生成目录不存在就创建
//        File gDir = new File(filePath + "_g");
//        if (!gDir.exists()) {
//            gDir.mkdir();
//        }
//
//        // 转换
//        for (SysFile sysFile : sysFiles) {
//            System.out.println(sysFile.getUrl());
//            try {
//                String url = imageToImage.generate(sysFile.getUrl());
//                byte[] bytes = FileUtils.downloadPicture(url);
//                FileUtils.byteToFile(bytes, filePath + "_g/" + sysFile.getFileName());
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//
//        }
//    }
//}
