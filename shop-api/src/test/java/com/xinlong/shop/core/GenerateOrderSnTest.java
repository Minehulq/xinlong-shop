package com.xinlong.shop.core;

import com.xinlong.shop.core.util.OrderSnTypeEnum;
import com.xinlong.shop.core.util.OrderUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 17:37 2023/6/13
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class GenerateOrderSnTest {

    @Resource
    private OrderUtil orderUtil;

    @Test
    public void generateTest() {

        for (int i = 0; i < 1000; i++) {
            String orderSn = orderUtil.getOrderSn(OrderSnTypeEnum.ORDER.getCode());
            System.out.println(orderSn);
        }

    }
}
