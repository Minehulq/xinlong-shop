package com.xinlong.shop.core.blind.entity.vo;

import com.xinlong.shop.core.blind.entity.BlindBoxOrderItem;
import com.xinlong.shop.core.blind.entity.dto.BlindBoxOrderDTO;
import io.swagger.annotations.ApiModel;

import java.util.List;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 15:12 2023/6/29
 */
@ApiModel(description = "盲盒订单VO")
public class BlindBoxOrderVO extends BlindBoxOrderDTO {

    private List<BlindBoxOrderItem> itemList;

    public List<BlindBoxOrderItem> getItemList() {
        return itemList;
    }

    public void setItemList(List<BlindBoxOrderItem> itemList) {
        this.itemList = itemList;
    }
}
