package com.xinlong.shop.core.goods.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xinlong.shop.core.goods.entity.GoodsClassify;
import com.xinlong.shop.core.goods.entity.vo.BuyerGoodsClassifyVO;
import com.xinlong.shop.core.goods.mapper.GoodsClassifyMapper;
import com.xinlong.shop.core.goods.mapstruct.GoodsClassifyStruct;
import com.xinlong.shop.core.goods.service.IGoodsClassifyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xinlong.shop.framework.cache.ICache;
import com.xinlong.shop.framework.util.FrameworkCacheEnum;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2022-11-04
 */
@Service
public class GoodsClassifyServiceImpl extends ServiceImpl<GoodsClassifyMapper, GoodsClassify> implements IGoodsClassifyService {

    private final ICache cache;
    private final GoodsClassifyStruct goodsClassifyStruct;

    public GoodsClassifyServiceImpl(ICache cache, GoodsClassifyStruct goodsClassifyStruct) {
        this.cache = cache;
        this.goodsClassifyStruct = goodsClassifyStruct;
    }

    @Override
    public void add(GoodsClassify goodsClassify) {
        this.save(goodsClassify);

        // 更新后删除前端缓存
        this.cache.delete(FrameworkCacheEnum.ALL_GOODS_CLASSIFY.toString());
    }

    @Override
    public void update(GoodsClassify goodsClassify, Integer id) {
        UpdateWrapper<GoodsClassify> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(goodsClassify, updateWrapper);

        // 更新后删除前端缓存
        this.cache.delete(FrameworkCacheEnum.ALL_GOODS_CLASSIFY.toString());
    }
    @Override
    public void delete(Integer id) {
        UpdateWrapper<GoodsClassify> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);

        // 更新后删除前端缓存
        this.cache.delete(FrameworkCacheEnum.ALL_GOODS_CLASSIFY.toString());
    }

    @Override
    public List<GoodsClassify> findByPid(Integer pid) {
        QueryWrapper<GoodsClassify> query = new QueryWrapper<>();
        query.eq("pid", pid);
        query.eq("disable", 0);
        query.orderByAsc("sort");
        return this.list(query);
    }

    @Override
    public void updateShow(Integer id, Integer isShow) {
        UpdateWrapper<GoodsClassify> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        GoodsClassify goodsClassify = new GoodsClassify();
        goodsClassify.setIsShow(isShow);
        this.update(goodsClassify, updateWrapper);
        // 更新后删除前端缓存
        this.cache.delete(FrameworkCacheEnum.ALL_GOODS_CLASSIFY.toString());
    }

    @Override
    public List<BuyerGoodsClassifyVO> findListChildren() {
        //FrameworkCacheEnum.ALL_GOODS_CLASSIFY

        // 所有的分类
        // 1. 先从缓存中获取
        List<GoodsClassify> goodsClassifys = (List<GoodsClassify>) this.cache.get(FrameworkCacheEnum.ALL_GOODS_CLASSIFY.toString());
        if (goodsClassifys == null) {
            QueryWrapper<GoodsClassify> query = new QueryWrapper<>();
            query.eq("is_show", 0);
            query.eq("disable", 0);
            query.orderByAsc("sort");
            goodsClassifys = this.list(query);
            cache.put(FrameworkCacheEnum.ALL_GOODS_CLASSIFY.toString(), goodsClassifys);
        }

        // 2. 递归数据
        List<BuyerGoodsClassifyVO> childrens = getChildren(goodsClassifys,0);

        return childrens;
    }

    /**
     * 递归查询子分类
     * @param list 分类集合
     * @param pid 父id
     * @return
     */
    private List<BuyerGoodsClassifyVO> getChildren(List<GoodsClassify> list, Integer pid) {
        List<BuyerGoodsClassifyVO> childrens = new ArrayList<>();
        for (GoodsClassify goodsClassify : list) {
            if (goodsClassify.getPid().equals(pid)) {
                BuyerGoodsClassifyVO buyerGoodsClassifyVO = goodsClassifyStruct.toBuyerGoodsClassifyVO(goodsClassify);
                buyerGoodsClassifyVO.setChildren(getChildren(list, buyerGoodsClassifyVO.getId()));
                childrens.add(buyerGoodsClassifyVO);
            }
        }
        return childrens;
    }

}
