package com.xinlong.shop.core.site.mapper;

import com.xinlong.shop.core.site.entity.Logistics;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 物流公司 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-05-26
 */
@Mapper
public interface LogisticsMapper extends BaseMapper<Logistics> {

}
