package com.xinlong.shop.core.site.service;

import com.xinlong.shop.core.site.entity.Article;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 文章列表 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-04-13
 */
public interface IArticleService extends IService<Article> {

    void update(Article article, Integer id);

    void delete(Integer id);

    void updateStatus(Integer id, Integer status);
}
