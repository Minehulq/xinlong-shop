package com.xinlong.shop.core.promotion.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xinlong.shop.core.promotion.entity.RushPurchaseCart;
import com.xinlong.shop.core.promotion.entity.RushPurchaseDetail;
import com.xinlong.shop.core.promotion.entity.dto.RushPurchaseCartDTO;
import com.xinlong.shop.core.promotion.mapper.RushPurchaseCartDTOMapper;
import com.xinlong.shop.core.promotion.mapper.RushPurchaseCartMapper;
import com.xinlong.shop.core.promotion.service.IRushPurchaseCartService;
import com.xinlong.shop.framework.core.entity.Member;
import com.xinlong.shop.framework.exception.ServiceException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 抢购购物车 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2023-05-05
 */
@Service
public class RushPurchaseCartServiceImpl extends ServiceImpl<RushPurchaseCartMapper, RushPurchaseCart> implements IRushPurchaseCartService {

    private final RushPurchaseDetailServiceImpl rushPurchaseDetailService;

    private final RushPurchaseCartDTOMapper rushPurchaseCartDTOMapper;

    public RushPurchaseCartServiceImpl(RushPurchaseDetailServiceImpl rushPurchaseDetailService, RushPurchaseCartDTOMapper rushPurchaseCartDTOMapper) {
        this.rushPurchaseDetailService = rushPurchaseDetailService;
        this.rushPurchaseCartDTOMapper = rushPurchaseCartDTOMapper;
    }

    @Override
    public void update(RushPurchaseCart rushPurchaseCart, Integer id) {
        UpdateWrapper<RushPurchaseCart> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(rushPurchaseCart, updateWrapper);
    }
    @Override
    public void delete(Integer id) {
        UpdateWrapper<RushPurchaseCart> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

    @Override
    public synchronized boolean add(RushPurchaseCart rushPurchaseCart) {
        boolean isHave = this.isHave(rushPurchaseCart.getRushPurchaseDetailId());
        if (isHave) {
            throw new ServiceException("该商品已被抢购");
        }
        // 判断是否有预订
        RushPurchaseDetail rpd = rushPurchaseDetailService.getById(rushPurchaseCart.getRushPurchaseDetailId());
        if (rpd.getBookingMemberId() != null && rpd.getBookingMemberId().intValue() != 0) {
            if (rpd.getBookingMemberId().intValue() != rushPurchaseCart.getMemberId().intValue()){
                throw new ServiceException("该商品已被预订");
            }

        }
        rushPurchaseCart.setCreateTime(DateUtil.currentSeconds());
        return this.save(rushPurchaseCart);
    }

    @Override
    public boolean isHave(Integer rushPurchaseDetailId) {
        QueryWrapper<RushPurchaseCart> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("rush_purchase_detail_id", rushPurchaseDetailId);
        List<RushPurchaseCart> list = this.list(queryWrapper);
        if (list.size() > 1) {
            throw new ServiceException("系统异常：购物车重复，请联系管理员");
        }
        if (list.size() == 1) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<RushPurchaseCart> listByMemberId(Integer memberId) {
        QueryWrapper<RushPurchaseCart> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("member_id", memberId);
        queryWrapper.orderByDesc("create_time");
        return this.list(queryWrapper);
    }

    @Override
    public void deleteByMemberId(Integer memberId) {
        UpdateWrapper<RushPurchaseCart> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("member_id", memberId);
        this.remove(deleteWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String batchBuy(Member member) {
        List<RushPurchaseCart> list = this.listByMemberId(member.getId());

        for(RushPurchaseCart rushPurchaseCart : list){
            String result = this.rushPurchaseDetailService.buy(rushPurchaseCart.getRushPurchaseDetailId(), member);
            if ("success".equals(result)) {
                this.delete(rushPurchaseCart.getId());
            } else {
                return result;
            }
        }
        return "success";

    }

    @Override
    public int countByMemberId(Integer memberId) {
        QueryWrapper<RushPurchaseCart> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("member_id", memberId);
        return (int) this.count(queryWrapper);
    }

    @Override
    public IPage<RushPurchaseCartDTO> dtoPage(IPage<RushPurchaseCartDTO> page, String goodsName) {
        return rushPurchaseCartDTOMapper.page(page, goodsName);
    }


}
