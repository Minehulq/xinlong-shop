package com.xinlong.shop.core.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xinlong.shop.core.member.entity.dto.MemberSignListDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 会员签名记录表 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-04-14
 */
@Mapper
public interface MemberSignListDTOMapper extends BaseMapper<MemberSignListDTO> {

    IPage<MemberSignListDTO> findPage(IPage<MemberSignListDTO> page,  @Param("status") Integer status);


}
