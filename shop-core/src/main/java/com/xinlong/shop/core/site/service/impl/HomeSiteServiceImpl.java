package com.xinlong.shop.core.site.service.impl;

import com.xinlong.shop.core.site.entity.HomeSite;
import com.xinlong.shop.core.site.mapper.HomeSiteMapper;
import com.xinlong.shop.core.site.service.IHomeSiteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;

/**
 * <p>
 * 首页位置 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2023-10-08
 */
@Service
public class HomeSiteServiceImpl extends ServiceImpl<HomeSiteMapper, HomeSite> implements IHomeSiteService {

    @Override
    public void update(HomeSite homeSite, Integer id) {
        UpdateWrapper<HomeSite> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(homeSite, updateWrapper);
    }
    @Override
    public void delete(Integer id) {
        UpdateWrapper<HomeSite> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

}
