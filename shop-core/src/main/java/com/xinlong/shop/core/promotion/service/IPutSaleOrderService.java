package com.xinlong.shop.core.promotion.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.core.promotion.entity.PutSaleOrder;

import java.util.List;

/**
 * <p>
 * 上架订单列表 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-03-29
 */
public interface IPutSaleOrderService extends IService<PutSaleOrder> {

    boolean update(PutSaleOrder putSaleOrder, Integer id);

    void delete(Integer id);

    /**
     * 根据活动详情的id来查询上架订单
     * @param detailId
     */
    PutSaleOrder findByDetailId(Integer detailId);

    /**
     * 上架商品（支付完成后 回调使用，也可后台界面直接调用）
     * @param orderSn 支付的订单号
     * @param payOrderSn 第三方支付单号
     */
    void putSaleOrder(String orderSn, String payOrderSn);


    /**
     * 保存上架订单
     * @param putSaleOrder
     * @return
     */
    boolean savePutSaleOrder(PutSaleOrder putSaleOrder);

    /**
     * 根据会员id查询未上架的上架订单
     * @param memberId
     * @return
     */
    List<PutSaleOrder> findByMemberId(Integer memberId);

}
