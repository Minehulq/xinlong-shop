package com.xinlong.shop.core.site.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xinlong.shop.core.site.entity.Adv;
import com.xinlong.shop.core.site.entity.AdvPromotion;
import com.xinlong.shop.core.site.entity.dto.AdvListDTO;
import com.xinlong.shop.core.site.mapper.AdvMapper;
import com.xinlong.shop.core.site.service.IAdvPromotionService;
import com.xinlong.shop.core.site.service.IAdvService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 广告表 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2023-05-26
 */
@Service
public class AdvServiceImpl extends ServiceImpl<AdvMapper, Adv> implements IAdvService {

    private final IAdvPromotionService advPromotionService;

    public AdvServiceImpl(IAdvPromotionService advPromotionService) {
        this.advPromotionService = advPromotionService;
    }

    @Override
    public void update(Adv adv, Integer id) {
        UpdateWrapper<Adv> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(adv, updateWrapper);
    }
    @Override
    public void delete(Integer id) {
        UpdateWrapper<Adv> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

    @Override
    public IPage<AdvListDTO> pageByPromotionId(IPage<AdvListDTO> page, Integer promotionId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(promotionId != null && promotionId.intValue() != 0, "promotion_id", promotionId);
        return this.baseMapper.pageByPromotionId(page, queryWrapper);
    }

    @Override
    public List<Adv> listByAdvPromotionCode(String advPromotionCode) {
        AdvPromotion advPromotion = this.advPromotionService.getByCode(advPromotionCode);
        if (advPromotion == null) {
            return null;
        }
        QueryWrapper<Adv> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("promotion_id", advPromotion.getId());
        return this.list(queryWrapper);
    }

}
