package com.xinlong.shop.core.member.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.core.member.entity.MemberBalanceDetail;
import com.xinlong.shop.core.member.entity.vo.MemberBalanceDetailVO;

import java.math.BigDecimal;

/**
 * <p>
 * 用户余额明细 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-04-06
 */
public interface IMemberBalanceDetailService extends IService<MemberBalanceDetail> {

    void update(MemberBalanceDetail memberBalanceDetail, Integer id);

    void delete(Integer id);

    /**
     * 新增操作记录
     * @param memberId
     * @param source
     * @param type
     * @param price
     * @param memo 备注
     */
    void add(Integer memberId, String source, Integer type, BigDecimal price, String memo);

    /**
     * 通过来源查询
     * @param source
     * @return
     */
    MemberBalanceDetail findBySource(String source);

    IPage<MemberBalanceDetail> page(IPage<MemberBalanceDetail> page, Integer memberId);

    /**
     * 查询余额收益总额
     * @param memberId
     * @return
     */
    BigDecimal findBalanceTotalByMemberId(Integer memberId);

    IPage<MemberBalanceDetailVO> pageByVo(IPage<MemberBalanceDetailVO> page, String mobile);
}
