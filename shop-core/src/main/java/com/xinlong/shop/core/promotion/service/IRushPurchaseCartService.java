package com.xinlong.shop.core.promotion.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.core.promotion.entity.RushPurchaseCart;
import com.xinlong.shop.core.promotion.entity.dto.RushPurchaseCartDTO;
import com.xinlong.shop.framework.core.entity.Member;

import java.util.List;

/**
 * <p>
 * 抢购购物车 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-05-05
 */
public interface IRushPurchaseCartService extends IService<RushPurchaseCart> {

    void update(RushPurchaseCart rushPurchaseCart, Integer id);

    void delete(Integer id);

    boolean add(RushPurchaseCart rushPurchaseCart);

    boolean isHave(Integer rushPurchaseDetailId);

    List<RushPurchaseCart> listByMemberId(Integer memberId);

    void deleteByMemberId(Integer memberId);

    String batchBuy(Member member);

    int countByMemberId(Integer memberId);

    IPage<RushPurchaseCartDTO> dtoPage(IPage<RushPurchaseCartDTO> page, String goodsName);

}
