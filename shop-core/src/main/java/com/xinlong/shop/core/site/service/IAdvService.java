package com.xinlong.shop.core.site.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.core.site.entity.Adv;
import com.xinlong.shop.core.site.entity.dto.AdvListDTO;

import java.util.List;

/**
 * <p>
 * 广告表 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-05-26
 */
public interface IAdvService extends IService<Adv> {

    void update(Adv adv, Integer id);

    void delete(Integer id);

    IPage<AdvListDTO> pageByPromotionId(IPage<AdvListDTO> page, Integer promotionId);

    List<Adv> listByAdvPromotionCode(String advPromotionCode);
}
