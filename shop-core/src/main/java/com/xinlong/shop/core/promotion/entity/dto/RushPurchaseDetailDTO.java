package com.xinlong.shop.core.promotion.entity.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.xinlong.shop.core.promotion.entity.RushPurchaseDetail;

import java.util.List;

/**
  * @Author Sylow
  * @Description
  * @Date: Created in 12:40 2023/3/8
  */
@TableName(autoResultMap = true)
public class RushPurchaseDetailDTO extends RushPurchaseDetail {

    /**
     * 开始时间
     */
    private Long startTime;

    /**
     * 结束时间
     */
    private Long endTime;

    /**
     * 商品简介
     */
    private String goodsDesc;

    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> goodsImgs;

    /**
     * 商品详情
     */
    private String goodsIntro;

    /**
     * 购物车id
     */
    private Integer cartId;


    public String getGoodsDesc() {
        return goodsDesc;
    }

    public void setGoodsDesc(String goodsDesc) {
        this.goodsDesc = goodsDesc;
    }

    public List<String> getGoodsImgs() {
        return goodsImgs;
    }

    public void setGoodsImgs(List<String> goodsImgs) {
        this.goodsImgs = goodsImgs;
    }

    public String getGoodsIntro() {
        return goodsIntro;
    }

    public void setGoodsIntro(String goodsIntro) {
        this.goodsIntro = goodsIntro;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Integer getCartId() {
        return cartId;
    }

    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }
}
