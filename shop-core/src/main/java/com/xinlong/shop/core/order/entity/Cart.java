package com.xinlong.shop.core.order.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 购物车表
 * </p>
 *
 * @author Sylow
 * @since 2023-04-21
 */
@TableName("s_cart")
public class Cart implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品id
     */
    @NotNull(message = "商品不能为空")
    private Integer goodsId;

    /**
     * 数量
     */
    @NotNull(message = "商品数量不能为空")
    private Integer num;

    /**
     * 会员id
     */
    private Integer memberId;

    /**
     * 更新时间
     */
    private Long updateTime;

    /**
     * 创建时间
     */
    private Long createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }
    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }
    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }
    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }
    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "Cart{" +
            "id=" + id +
            ", goodsId=" + goodsId +
            ", num=" + num +
            ", memberId=" + memberId +
            ", updateTime=" + updateTime +
            ", createTime=" + createTime +
        "}";
    }
}
