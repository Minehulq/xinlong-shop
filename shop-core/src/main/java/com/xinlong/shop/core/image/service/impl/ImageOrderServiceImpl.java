package com.xinlong.shop.core.image.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xinlong.shop.core.image.entity.ImageOrder;
import com.xinlong.shop.core.image.mapper.ImageOrderMapper;
import com.xinlong.shop.core.image.service.IImageOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xinlong.shop.core.util.OrderSnTypeEnum;
import com.xinlong.shop.core.util.OrderUtil;
import com.xinlong.shop.framework.exception.ServiceException;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;

import java.math.BigDecimal;

/**
 * <p>
 * 图片下载订单 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2023-08-04
 */
@Service
public class ImageOrderServiceImpl extends ServiceImpl<ImageOrderMapper, ImageOrder> implements IImageOrderService {

    private final OrderUtil orderUtil;

    public ImageOrderServiceImpl(OrderUtil orderUtil) {
        this.orderUtil = orderUtil;
    }

    @Override
    public void update(ImageOrder imageOrder, Integer id) {
        UpdateWrapper<ImageOrder> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(imageOrder, updateWrapper);
    }
    @Override
    public void delete(Integer id) {
        UpdateWrapper<ImageOrder> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

    @Override
    public ImageOrder createOrder(Integer memberId, String imageUrl) {
        ImageOrder imageOrder = new ImageOrder();
        imageOrder.setMemberId(memberId);
        imageOrder.setImageUrl(imageUrl);
        imageOrder.setPrice(BigDecimal.ONE);
        imageOrder.setOrderSn(orderUtil.getOrderSn(OrderSnTypeEnum.IMAGE_ORDER.getCode()));
        imageOrder.setStatus(0);
        imageOrder.setCreateTime(DateUtil.currentSeconds());
        boolean save = this.save(imageOrder);
        if (save) {
            return imageOrder;
        } else {
            throw new ServiceException("创建订单失败");
        }
    }

    @Override
    public IPage<ImageOrder> selectPage(IPage<ImageOrder> page, Integer memberId) {
        QueryWrapper<ImageOrder> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("member_id", memberId);
        queryWrapper.eq("status", 1);
        queryWrapper.orderByDesc("create_time");
        return this.baseMapper.selectPage(page, queryWrapper);
    }

    @Override
    public void paySuccess(String orderSn, String payOrderSn) {
        ImageOrder imageOrder = this.baseMapper.selectOne(new QueryWrapper<ImageOrder>().eq("order_sn", orderSn));
        if (imageOrder == null) {
            throw new ServiceException("订单不存在");
        }
        imageOrder.setStatus(1);
        imageOrder.setPayOrderSn(payOrderSn);
        this.update(imageOrder, imageOrder.getId());
    }

}
