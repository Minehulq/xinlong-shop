package com.xinlong.shop.core.goods.mapper;

import com.xinlong.shop.core.goods.entity.GoodsClassify;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2022-11-04
 */
@Mapper
public interface GoodsClassifyMapper extends BaseMapper<GoodsClassify> {

}
