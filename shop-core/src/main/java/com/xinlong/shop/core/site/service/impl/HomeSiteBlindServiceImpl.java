package com.xinlong.shop.core.site.service.impl;

import cn.hutool.core.date.DateUtil;
import com.xinlong.shop.core.blind.entity.BlindBox;
import com.xinlong.shop.core.site.entity.HomeSiteBlind;
import com.xinlong.shop.core.site.entity.dto.HomeBlindBoxDTO;
import com.xinlong.shop.core.site.entity.vo.HomeSiteBlindVO;
import com.xinlong.shop.core.site.mapper.HomeSiteBlindMapper;
import com.xinlong.shop.core.site.service.IHomeSiteBlindService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 首页显示的盲盒商品 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2023-10-08
 */
@Service
public class HomeSiteBlindServiceImpl extends ServiceImpl<HomeSiteBlindMapper, HomeSiteBlind> implements IHomeSiteBlindService {

    @Override
    public void update(HomeSiteBlind homeSiteBlind, Integer id) {
        UpdateWrapper<HomeSiteBlind> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(homeSiteBlind, updateWrapper);
    }
    @Override
    public void delete(Integer id) {
        UpdateWrapper<HomeSiteBlind> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchSaveBlindBox(HomeSiteBlindVO homeSiteBlindVO) {
        // 先清空再新增
        this.clearBlindBox(homeSiteBlindVO.getSiteCode());
        List<BlindBox> blindBoxList = homeSiteBlindVO.getBlindBoxList();
        List<HomeSiteBlind> list = new ArrayList<>();
        blindBoxList.forEach(blindBox -> {
            HomeSiteBlind homeSiteBlind = new HomeSiteBlind();
            homeSiteBlind.setBlindGoodsId(blindBox.getId());
            homeSiteBlind.setCreateTime(DateUtil.currentSeconds());
            homeSiteBlind.setSiteId(homeSiteBlindVO.getId());
            homeSiteBlind.setSiteCode(homeSiteBlindVO.getSiteCode());
            homeSiteBlind.setSort(0);
            list.add(homeSiteBlind);
        });
        this.saveBatch(list);
    }

    @Override
    public List<HomeBlindBoxDTO> findBlindBoxList(String siteCode) {
        return this.baseMapper.findHomeBlindBoxDTO(siteCode);
    }

    @Override
    public void clearBlindBox(String siteCode) {
        UpdateWrapper<HomeSiteBlind> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("site_code", siteCode);
        this.remove(deleteWrapper);
    }

}
