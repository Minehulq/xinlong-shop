package com.xinlong.shop.core.member.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.core.member.MemberWithdrawalQueryParam;
import com.xinlong.shop.core.member.entity.MemberWithdrawal;
import com.xinlong.shop.core.member.entity.dto.MemberWithdrawalDTO;

import java.math.BigDecimal;

/**
 * <p>
 * 用户提现记录 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-04-06
 */
public interface IMemberWithdrawalService extends IService<MemberWithdrawal> {

    void update(MemberWithdrawal memberWithdrawal, Integer id);

    void delete(Integer id);

    Page<MemberWithdrawal> page(Page<MemberWithdrawal> page, Integer memberId);

    Page<MemberWithdrawalDTO> page(Page<MemberWithdrawalDTO> page, MemberWithdrawalQueryParam memberWithdrawalQueryParam);

    /**
     * 查询余额累计提现总额
     * @param memberId
     * @return
     */
    BigDecimal findTotalByMemberId(Integer memberId);

    /**
     * 新增一条提现记录
     * @param memberWithdrawal
     */
    void add(MemberWithdrawal memberWithdrawal);

    void updateStatus(Integer id, Integer status);

    /**
     * 审核拒绝 退回用户余额
     * @param id
     */
    void refuse(Integer id);
}
