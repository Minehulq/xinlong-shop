package com.xinlong.shop.core.site.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

/**
 * <p>
 * 首页显示的盲盒商品
 * </p>
 *
 * @author Sylow
 * @since 2023-10-08
 */
@TableName("sys_home_site_blind")
public class HomeSiteBlind implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 位置ID
     */
    private Integer siteId;

    /**
     * 位置code
     */
    private String siteCode;

    /**
     * 盲盒商品id
     */
    private Integer blindGoodsId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 创建时间
     */
    private Long createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getSiteId() {
        return siteId;
    }

    public void setSiteId(Integer siteId) {
        this.siteId = siteId;
    }
    public String getSiteCode() {
        return siteCode;
    }

    public void setSiteCode(String siteCode) {
        this.siteCode = siteCode;
    }
    public Integer getBlindGoodsId() {
        return blindGoodsId;
    }

    public void setBlindGoodsId(Integer blindGoodsId) {
        this.blindGoodsId = blindGoodsId;
    }
    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "HomeSiteBlind{" +
            "id=" + id +
            ", siteId=" + siteId +
            ", siteCode=" + siteCode +
            ", blindGoodsId=" + blindGoodsId +
            ", sort=" + sort +
            ", createTime=" + createTime +
        "}";
    }
}
