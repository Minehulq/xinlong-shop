package com.xinlong.shop.core.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.core.member.MemberWithdrawalQueryParam;
import com.xinlong.shop.core.member.entity.dto.MemberWithdrawalDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 用户提现记录DTO Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-04-06
 */
@Mapper
public interface MemberWithdrawalDTOMapper extends BaseMapper<MemberWithdrawalDTO> {

    Page<MemberWithdrawalDTO> findByPage(Page<MemberWithdrawalDTO> page, @Param("queryParam") MemberWithdrawalQueryParam queryParam);

}
