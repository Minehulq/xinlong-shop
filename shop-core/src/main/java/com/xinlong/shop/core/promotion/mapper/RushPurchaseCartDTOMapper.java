package com.xinlong.shop.core.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xinlong.shop.core.promotion.entity.dto.RushPurchaseCartDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 11:45 2023/5/15
 */
@Mapper
public interface RushPurchaseCartDTOMapper extends BaseMapper<RushPurchaseCartDTO> {

    IPage<RushPurchaseCartDTO> page(IPage<RushPurchaseCartDTO> page, @Param("GoodsName") String goodsName);


}
