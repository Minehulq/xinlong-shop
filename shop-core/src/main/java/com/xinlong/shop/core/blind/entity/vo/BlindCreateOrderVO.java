package com.xinlong.shop.core.blind.entity.vo;

import java.math.BigDecimal;

/**
 * @Author Sylow
 * @Description 用户端创建订单时使用
 * @Date: Created in 11:42 2023/6/14
 */
public class BlindCreateOrderVO {

    private Integer blindBoxId;

    private String blindBoxName;

    /**
     * 单价
     */
    private BigDecimal unitPrice;

    /**
     * 总价
     */
    private BigDecimal totalPrice;

    /**
     * 盲盒抽奖次数
     */
    private Integer blindBoxNum;


    public Integer getBlindBoxId() {
        return blindBoxId;
    }

    public void setBlindBoxId(Integer blindBoxId) {
        this.blindBoxId = blindBoxId;
    }

    public String getBlindBoxName() {
        return blindBoxName;
    }

    public void setBlindBoxName(String blindBoxName) {
        this.blindBoxName = blindBoxName;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Integer getBlindBoxNum() {
        return blindBoxNum;
    }

    public void setBlindBoxNum(Integer blindBoxNum) {
        this.blindBoxNum = blindBoxNum;
    }
}
