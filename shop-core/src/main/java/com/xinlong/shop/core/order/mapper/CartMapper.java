package com.xinlong.shop.core.order.mapper;

import com.xinlong.shop.core.order.entity.Cart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 购物车表 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-04-21
 */
@Mapper
public interface CartMapper extends BaseMapper<Cart> {

}
