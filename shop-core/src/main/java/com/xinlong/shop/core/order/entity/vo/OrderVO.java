package com.xinlong.shop.core.order.entity.vo;

import com.xinlong.shop.core.order.entity.Order;
import com.xinlong.shop.core.order.entity.OrderItem;

import java.util.List;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 17:49 2023/7/5
 */
public class OrderVO extends Order {

    private List<OrderItem> orderItems;

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }
}
