package com.xinlong.shop.core.image.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.core.image.entity.ImageOrder;

/**
 * <p>
 * 图片下载订单 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-08-04
 */
public interface IImageOrderService extends IService<ImageOrder> {

    void update(ImageOrder imageOrder, Integer id);

    void delete(Integer id);

    ImageOrder createOrder(Integer memberId, String imageUrl);

    /**
     * 根据会员查询订单分页
     * @param page
     * @param memberId
     * @return
     */
    IPage<ImageOrder> selectPage(IPage<ImageOrder> page, Integer memberId);

    void paySuccess(String orderSn, String payOrderSn);
}
