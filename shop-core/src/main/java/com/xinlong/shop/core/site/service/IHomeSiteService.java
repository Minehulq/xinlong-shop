package com.xinlong.shop.core.site.service;

import com.xinlong.shop.core.site.entity.HomeSite;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 首页位置 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-10-08
 */
public interface IHomeSiteService extends IService<HomeSite> {

    void update(HomeSite homeSite, Integer id);

    void delete(Integer id);
}
