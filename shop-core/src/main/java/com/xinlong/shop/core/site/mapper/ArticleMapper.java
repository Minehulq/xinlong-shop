package com.xinlong.shop.core.site.mapper;

import com.xinlong.shop.core.site.entity.Article;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 文章列表 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-04-13
 */
@Mapper
public interface ArticleMapper extends BaseMapper<Article> {

}
