package com.xinlong.shop.core.member.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * <p>
 * 会员收款方式
 * </p>
 *
 * @author Sylow
 * @since 2023-03-15
 */
@TableName("s_member_payment")
public class MemberPayment implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主健
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 会员id
     */
    private Integer memberId;

    /**
     * 银行代码
     */
    private String bankCode;

    /**
     * 银行名称
     */
    private String bankName;

    /**
     * 账户名称
     */
    private String accountName;

    /**
     * 账号
     */
    private String accountNumber;

    /**
     * 支付宝收款码
     */
    private String alipay;

    /**
     * 微信收款码
     */
    private String wxpay;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }
    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }
    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }
    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
    public String getAlipay() {
        return alipay;
    }

    public void setAlipay(String alipay) {
        this.alipay = alipay;
    }
    public String getWxpay() {
        return wxpay;
    }

    public void setWxpay(String wxpay) {
        this.wxpay = wxpay;
    }

    @Override
    public String toString() {
        return "MemberPayment{" +
            "id=" + id +
            ", memberId=" + memberId +
            ", bankCode=" + bankCode +
            ", bankName=" + bankName +
            ", accountName=" + accountName +
            ", accountNumber=" + accountNumber +
            ", alipay=" + alipay +
            ", wxpay=" + wxpay +
        "}";
    }
}
