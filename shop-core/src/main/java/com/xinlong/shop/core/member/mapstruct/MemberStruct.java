package com.xinlong.shop.core.member.mapstruct;

import com.xinlong.shop.core.member.entity.dto.MemberDTO;
import com.xinlong.shop.framework.core.entity.Member;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 21:08 2022/11/6
 */
@Mapper(componentModel = "spring")
public interface MemberStruct {

    MemberStruct INSTANCE = Mappers.getMapper(MemberStruct.class);

    //@Mapping(target = "memberNo", source = "mobile", qualifiedByName = "convertMemberNo")
    Member toMember(MemberDTO memberDTO);

    MemberDTO toMemberDTO(Member member);
//    测试用的
//    @Named("convertMemberNo")
//    static String convertMemberNo(String mobile) {
//        return "m_" + mobile;
//    }
}
