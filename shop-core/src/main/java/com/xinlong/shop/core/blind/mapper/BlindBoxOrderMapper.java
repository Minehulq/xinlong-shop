package com.xinlong.shop.core.blind.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.xinlong.shop.core.blind.entity.BlindBoxOrder;
import com.xinlong.shop.core.blind.entity.dto.BlindBoxOrderDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * <p>
 * 盲盒订单表 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-06-13
 */
@Mapper
public interface BlindBoxOrderMapper extends BaseMapper<BlindBoxOrder> {

    @Select("SELECT bo.*,b.blind_box_name,m.member_name,m.mobile AS member_mobile,m.nickname " +
            "FROM s_blind_box_order bo " +
            "LEFT JOIN s_member m ON bo.member_id=m.id " +
            "LEFT JOIN s_blind_box b ON bo.blind_box_id=b.id " +
            "${ew.customSqlSegment} ORDER BY bo.create_time DESC")
    IPage<BlindBoxOrderDTO> findByPage(IPage<BlindBoxOrderDTO> page, @Param(Constants.WRAPPER) Wrapper queryWrapper);

    @Select("SELECT bo.*,b.blind_box_name,m.member_name,m.mobile AS member_mobile,m.nickname " +
            "FROM s_blind_box_order bo " +
            "LEFT JOIN s_member m ON bo.member_id=m.id " +
            "LEFT JOIN s_blind_box b ON bo.blind_box_id=b.id " +
            "${ew.customSqlSegment} ORDER BY bo.create_time DESC")
    List<BlindBoxOrderDTO> findByList(@Param(Constants.WRAPPER) Wrapper queryWrapper);

    /**
     * 这个方法调用的话 判断条件内的盲盒订单下的商品 是否还有未提货的，如果没有了 就会把盲盒订单改为已提货
     * @param orderStatus
     * @param orderIds
     */
    @Update("UPDATE s_blind_box_order " +
            "SET STATUS=${OrderStatus} WHERE id NOT IN (" +
            "SELECT blind_box_order_id FROM s_blind_box_order_item WHERE blind_box_order_id IN (${OrderIds}) AND STATUS=1 GROUP BY blind_box_order_id) " +
            "AND id IN (${OrderIds})")
    void updateOrderStatus(@Param("OrderStatus") Integer orderStatus, @Param("OrderIds") String orderIds);
}
