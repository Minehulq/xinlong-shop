package com.xinlong.shop.core.member.entity.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @Author Sylow
 * @Description 会员dto 后台管理编辑会员使用
 * @Date: Created in 21:06 2022/11/6
 */
public class MemberDTO implements Serializable {

    //@NotBlank(message = "会员id不能为空")
    private Integer id;

    @NotBlank(message = "会员用户名不能为空")
    @Pattern(regexp = "^[a-zA-Z0-9_-]{5,16}$", message = "用户名必须是5到16位（字母，数字，下划线，减号）")
    private String memberName;

    private String memberNo;

//    @NotBlank(message = "密码不能为空")
//    @Length(min=6, max=20,message = "密码长度在6~20字符之间")
    private String password;

    @NotBlank(message = "手机号不能为空")
    @Pattern(regexp = "^1[0-9]{10}$", message = "手机号不合法")
    private String mobile;

    @NotBlank(message = "昵称不能为空")
    @Length(min=1, max=20,message = "昵称长度在20字符以内")
    private String nickname;

    private Integer gradeId;

    private Integer inviteMemberId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Integer getGradeId() {
        return gradeId;
    }

    public void setGradeId(Integer gradeId) {
        this.gradeId = gradeId;
    }

    public Integer getInviteMemberId() {
        return inviteMemberId;
    }

    public void setInviteMemberId(Integer inviteMemberId) {
        this.inviteMemberId = inviteMemberId;
    }
}
