package com.xinlong.shop.core.util;

/**
 * 订单编号类型 枚举类
 */
public enum OrderSnTypeEnum {

    ORDER(1),PURCHASE(2),BLIND_BOX(3),IMAGE_ORDER(4);

    private int code;
    private OrderSnTypeEnum(int code) {
        this.code = code;
    }

    public int getCode(){
        return code;
    }
}
