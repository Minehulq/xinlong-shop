package com.xinlong.shop.core.site.service.impl;

import com.xinlong.shop.core.site.entity.Article;
import com.xinlong.shop.core.site.mapper.ArticleMapper;
import com.xinlong.shop.core.site.service.IArticleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;

/**
 * <p>
 * 文章列表 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2023-04-13
 */
@Service
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper, Article> implements IArticleService {

    @Override
    public void update(Article article, Integer id) {
        UpdateWrapper<Article> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(article, updateWrapper);
    }
    @Override
    public void delete(Integer id) {
        UpdateWrapper<Article> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

    @Override
    public void updateStatus(Integer id, Integer status) {
        UpdateWrapper<Article> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        updateWrapper.set("status", status);
        this.update(updateWrapper);
    }

}
