package com.xinlong.shop.core.address.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.core.address.entity.MemberAddress;

import java.util.List;

/**
 * <p>
 * 会员收货地址 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-03-17
 */
public interface IMemberAddressService extends IService<MemberAddress> {

    void update(MemberAddress memberAddress, Integer id);

    void delete(Integer id, Integer memberId);

    List<MemberAddress> listByMemberId(Integer memberId);

    boolean saveAddress(MemberAddress memberAddress);

    long findAddressNum(Integer memberId);

    /**
     * 根据会员id获取默认地址（没有默认地址 获取第一个地址）
     * @param memberId
     * @return
     */
    MemberAddress getDefByMemberId(Integer memberId);
}
