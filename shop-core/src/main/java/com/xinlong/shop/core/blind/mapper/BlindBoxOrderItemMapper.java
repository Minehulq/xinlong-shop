package com.xinlong.shop.core.blind.mapper;

import com.xinlong.shop.core.blind.entity.BlindBoxOrderItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 盲盒订单项 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-06-27
 */
@Mapper
public interface BlindBoxOrderItemMapper extends BaseMapper<BlindBoxOrderItem> {

}
