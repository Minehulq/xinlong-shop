package com.xinlong.shop.core.blind.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

/**
 * <p>
 * 盲盒订单表
 * </p>
 *
 * @author Sylow
 * @since 2023-06-13
 */
@ApiModel(value="BlindBoxOrder对象", description="盲盒订单表")
@TableName("s_blind_box_order")
public class BlindBoxOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 订单编号
     */
    @ApiModelProperty(value = "订单编号")
    private String orderSn;

    /**
     * 盲盒id
     */
    private Integer blindBoxId;

    /**
     * 会员id
     */
    private Integer memberId;

    /**
     * 盲盒抽奖次数
     */
    @ApiModelProperty(value = "盲盒抽奖次数")
    private Integer blindBoxNum;

    /**
     * 订单总价
     */
    @ApiModelProperty(value = "订单总价")
    private BigDecimal price;

    /**
     * 价格规则
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private Map priceRule;

    /**
     * 订单状态
     */
    @ApiModelProperty(value = "订单状态")
    private Integer status;

    /**
     * 第三方支付单号
     */
    @ApiModelProperty(value = "第三方支付单号")
    private String paymentOrderNo;

    /**
     * 支付时间
     */
    @ApiModelProperty(value = "支付时间")
    private Long payTime;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Long createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }
    public Integer getBlindBoxId() {
        return blindBoxId;
    }

    public void setBlindBoxId(Integer blindBoxId) {
        this.blindBoxId = blindBoxId;
    }
    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }
    public Integer getBlindBoxNum() {
        return blindBoxNum;
    }

    public void setBlindBoxNum(Integer blindBoxNum) {
        this.blindBoxNum = blindBoxNum;
    }
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Map getPriceRule() {
        return priceRule;
    }

    public void setPriceRule(Map priceRule) {
        this.priceRule = priceRule;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public String getPaymentOrderNo() {
        return paymentOrderNo;
    }

    public void setPaymentOrderNo(String paymentOrderNo) {
        this.paymentOrderNo = paymentOrderNo;
    }
    public Long getPayTime() {
        return payTime;
    }

    public void setPayTime(Long payTime) {
        this.payTime = payTime;
    }
    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "BlindBoxOrder{" +
            "id=" + id +
            ", orderSn=" + orderSn +
            ", blindBoxId=" + blindBoxId +
            ", memberId=" + memberId +
            ", blindBoxNum=" + blindBoxNum +
            ", price=" + price +
            ", priceRule=" + priceRule +
            ", status=" + status +
            ", paymentOrderNo=" + paymentOrderNo +
            ", payTime=" + payTime +
            ", createTime=" + createTime +
        "}";
    }
}
