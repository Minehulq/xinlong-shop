package com.xinlong.shop.core.logistics.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @Author Sylow
 * @Description 物流信息
 * @Date: Created in 21:59 2023/7/27
 */
public class LogisticsTraces implements Serializable {

    public LogisticsTraces(){}

    public LogisticsTraces(String logisticsName, String logisticsSn, List<Map> traces) {
        this.logisticsName = logisticsName;
        this.logisticsSn = logisticsSn;
        this.traces = traces;
    }

    /**
     * 物流公司
     */
    private String logisticsName;

    /**
     * 物流单号
     */
    private String logisticsSn;

    /**
     * 物流轨迹
     */
    private List<Map> traces;

    public String getLogisticsName() {
        return logisticsName;
    }

    public void setLogisticsName(String logisticsName) {
        this.logisticsName = logisticsName;
    }

    public String getLogisticsSn() {
        return logisticsSn;
    }

    public void setLogisticsSn(String logisticsSn) {
        this.logisticsSn = logisticsSn;
    }

    public List<Map> getTraces() {
        return traces;
    }

    public void setTraces(List<Map> traces) {
        this.traces = traces;
    }
}
