package com.xinlong.shop.core.promotion.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 上架订单列表
 * </p>
 *
 * @author Sylow
 * @since 2023-03-29
 */
@TableName("s_put_sale_order")
public class PutSaleOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 订单号与抢购表一致
     */
    private String orderSn;

    /**
     * 活动场次id
     */
    private Integer rushPurchaseId;

    /**
     * 抢购活动明细id
     */
    private Integer rushPurchaseDetailId;

    /**
     * 上架后，下一场抢购活动明细id
     */
    private Integer nextRushPurchaseDetailId;

    /**
     * 原价（购买价）
     */
    private BigDecimal originalPrice;

    /**
     * 上架后新价格
     */
    private BigDecimal newPrice;

    /**
     * 服务费（上架费）
     */
    private BigDecimal servicePrice;

    /**
     * 第三方支付单号
     */
    private String paymentOrderNo;

    /**
     * 上架会员id
     */
    private Integer memberId;

    /**
     * 状态0默认1支付成功
     */
    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }
    public Integer getRushPurchaseId() {
        return rushPurchaseId;
    }

    public void setRushPurchaseId(Integer rushPurchaseId) {
        this.rushPurchaseId = rushPurchaseId;
    }
    public Integer getRushPurchaseDetailId() {
        return rushPurchaseDetailId;
    }

    public void setRushPurchaseDetailId(Integer rushPurchaseDetailId) {
        this.rushPurchaseDetailId = rushPurchaseDetailId;
    }
    public Integer getNextRushPurchaseDetailId() {
        return nextRushPurchaseDetailId;
    }

    public void setNextRushPurchaseDetailId(Integer nextRushPurchaseDetailId) {
        this.nextRushPurchaseDetailId = nextRushPurchaseDetailId;
    }
    public BigDecimal getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(BigDecimal originalPrice) {
        this.originalPrice = originalPrice;
    }
    public BigDecimal getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(BigDecimal newPrice) {
        this.newPrice = newPrice;
    }
    public BigDecimal getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(BigDecimal servicePrice) {
        this.servicePrice = servicePrice;
    }
    public String getPaymentOrderNo() {
        return paymentOrderNo;
    }

    public void setPaymentOrderNo(String paymentOrderNo) {
        this.paymentOrderNo = paymentOrderNo;
    }
    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "PutSaleOrder{" +
            "id=" + id +
            ", orderSn=" + orderSn +
            ", rushPurchaseId=" + rushPurchaseId +
            ", rushPurchaseDetailId=" + rushPurchaseDetailId +
            ", nextRushPurchaseDetailId=" + nextRushPurchaseDetailId +
            ", originalPrice=" + originalPrice +
            ", newPrice=" + newPrice +
            ", servicePrice=" + servicePrice +
            ", paymentOrderNo=" + paymentOrderNo +
            ", memberId=" + memberId +
            ", status=" + status +
        "}";
    }
}
