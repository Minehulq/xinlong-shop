package com.xinlong.shop.core.blind.entity;

public enum BlindBoxOrderStatusEnum {
    // 已下单、已支付、已提货、已完成
    BUY(0),PAY(1),PICK_UP(2),COMPLETE(3);

    private int code;
    private BlindBoxOrderStatusEnum(int code) {
        this.code = code;
    }

    public int getCode(){
        return code;
    }
}
