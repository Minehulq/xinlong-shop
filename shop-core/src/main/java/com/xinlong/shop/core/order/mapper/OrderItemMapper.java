package com.xinlong.shop.core.order.mapper;

import com.xinlong.shop.core.order.entity.OrderItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 订单子项表 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-07-04
 */
@Mapper
public interface OrderItemMapper extends BaseMapper<OrderItem> {

}
