package com.xinlong.shop.core.order.entity.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.xinlong.shop.core.order.entity.Cart;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 15:32 2023/4/21
 */
@TableName(autoResultMap = true)
public class CartDTO extends Cart {

    private String goodsName;

    private BigDecimal goodsPrice;

    private String goodsSn;

    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> goodsImgs;

    private Integer goodsStatus;

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public String getGoodsSn() {
        return goodsSn;
    }

    public void setGoodsSn(String goodsSn) {
        this.goodsSn = goodsSn;
    }

    public List<String> getGoodsImgs() {
        return goodsImgs;
    }

    public void setGoodsImgs(List<String> goodsImgs) {
        this.goodsImgs = goodsImgs;
    }

    public Integer getGoodsStatus() {
        return goodsStatus;
    }

    public void setGoodsStatus(Integer goodsStatus) {
        this.goodsStatus = goodsStatus;
    }
}
