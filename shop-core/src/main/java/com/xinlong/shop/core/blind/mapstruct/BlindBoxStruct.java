package com.xinlong.shop.core.blind.mapstruct;

import com.xinlong.shop.core.blind.entity.BlindBox;
import com.xinlong.shop.core.blind.entity.vo.BlindBoxVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 14:10 2023/5/24
 */
@Mapper(componentModel = "spring")
public interface BlindBoxStruct {

    BlindBoxStruct INSTANCE = Mappers.getMapper(BlindBoxStruct.class);

    BlindBox toBlindBox(BlindBoxVO blindBoxVO);

    BlindBoxVO toBlindBoxVO(BlindBox blindBox);
}
