package com.xinlong.shop.core.member.entity.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @Author Sylow
 * @Description 会员搜索dto
 * @Date: Created in 13:29 2022/11/7
 */
@ApiModel(value = "会员搜索参数")
public class MemberSearchDTO implements Serializable {


    @ApiModelProperty(value = "会员名")
    private String memberName = "";

    @ApiModelProperty(value = "手机号")
    private String mobile = "";

    @ApiModelProperty(value = "昵称(姓名)")
    private String nickname = "";

    @ApiModelProperty(value = "邀请人手机号")
    private String inviteMobile = "";

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getInviteMobile() {
        return inviteMobile;
    }

    public void setInviteMobile(String inviteMobile) {
        this.inviteMobile = inviteMobile;
    }
}
