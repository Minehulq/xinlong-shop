package com.xinlong.shop.core.member;

public enum MemberWithdrawalStatus {
    WAITING_REVIEW(0, "等待审核"),
    PASS(1, "审核通过"),
    REFUSE(2, "审核拒绝"),
    PAY(3, "打款成功"),
    CLOSE(4, "撤销（关闭）");

    private Integer code;
    private String desc;

    MemberWithdrawalStatus(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
