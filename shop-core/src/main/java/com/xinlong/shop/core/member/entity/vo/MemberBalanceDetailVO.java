package com.xinlong.shop.core.member.entity.vo;

import com.xinlong.shop.core.member.entity.MemberBalanceDetail;

import java.math.BigDecimal;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 00:29 2023/7/10
 */
public class MemberBalanceDetailVO extends MemberBalanceDetail {

    private String nickname;

    private String mobile;

    private BigDecimal balance;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
