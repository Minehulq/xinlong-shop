package com.xinlong.shop.core.site.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xinlong.shop.core.site.entity.AdvPromotion;
import com.xinlong.shop.core.site.mapper.AdvPromotionMapper;
import com.xinlong.shop.core.site.service.IAdvPromotionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;

/**
 * <p>
 * 广告位 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2023-05-26
 */
@Service
public class AdvPromotionServiceImpl extends ServiceImpl<AdvPromotionMapper, AdvPromotion> implements IAdvPromotionService {

    @Override
    public void update(AdvPromotion advPromotion, Integer id) {
        UpdateWrapper<AdvPromotion> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(advPromotion, updateWrapper);
    }
    @Override
    public void delete(Integer id) {
        UpdateWrapper<AdvPromotion> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

    @Override
    public AdvPromotion getByCode(String code) {
        QueryWrapper<AdvPromotion> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("promotion_code", code);
        return this.getOne(queryWrapper);
    }

}
