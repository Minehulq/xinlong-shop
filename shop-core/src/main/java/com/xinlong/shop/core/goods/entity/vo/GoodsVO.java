package com.xinlong.shop.core.goods.entity.vo;

import com.xinlong.shop.core.goods.entity.Goods;

import java.io.Serializable;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 13:34 2022/11/17
 */
public class GoodsVO extends Goods implements Serializable {

    /**
     * 商品分类名称
     */
    private String classifyName;

    public String getClassifyName() {
        return classifyName;
    }

    public void setClassifyName(String classifyName) {
        this.classifyName = classifyName;
    }

}
