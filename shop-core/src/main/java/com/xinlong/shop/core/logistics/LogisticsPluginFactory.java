package com.xinlong.shop.core.logistics;

import com.xinlong.shop.framework.util.SpringUtils;
import org.springframework.stereotype.Component;

/**
 * @Author Sylow
 * @Description 物流查询插件工厂
 * @Date: Created in 23:30 2023/7/27
 */
@Component
public class LogisticsPluginFactory {

    /**
     * 得到当前启用的物流插件
     * @return
     */
    public ILogisticsPlugin currentPlugin() {
        // 目前只实现了快递100，就暂时写死了
        return SpringUtils.getBean("kuaidi100PluginImpl");
    }

}
