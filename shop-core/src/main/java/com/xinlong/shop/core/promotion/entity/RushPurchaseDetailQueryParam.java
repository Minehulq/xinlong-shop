package com.xinlong.shop.core.promotion.entity;

/**
 * @Author Sylow
 * @Description 活动订单查询参数实体(实际上就是Detail查询）
 * @Date: Created in 10:53 2023/5/4
 */
public class RushPurchaseDetailQueryParam {

    private Integer rushPurchaseId;

    /**
     * 活动场次id
     */
    private String activityId;

    /**
     * 订单编号
     */
    private String orderSn;

    /**
     * 商品名称
     */
    private String goodsName;


    /**
     * 卖家会员名称
     */
    private String sellerName;

    /**
     * 卖家手机
     */
    private String sellerMobile;

    /**
     * 买家会员名称
     */
    private String buyerName;

    /**
     * 买家手机号
     */
    private String buyerMobile;

    /**
     * 要显示的状态
     * @see DetailStatusEnum
     */
    private Integer status;

    /**
     * 不显示的状态
     */
    private Integer filterStatus;

    /**
     * 是否提货 0 否 1 是
     */
    private Integer isPickUp;

    /**
     * 时间
     */
    private Long[] buyTime;

    /**
     * 排序字段名称
     */
    private String sortName = "create_time";

    /**
     * 排序类型 0 desc 1 asc
     * 默认 0
     */
    private String sortType = "desc";

    public Integer getRushPurchaseId() {
        return rushPurchaseId;
    }

    public void setRushPurchaseId(Integer rushPurchaseId) {
        this.rushPurchaseId = rushPurchaseId;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getSellerMobile() {
        return sellerMobile;
    }

    public void setSellerMobile(String sellerMobile) {
        this.sellerMobile = sellerMobile;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public String getBuyerMobile() {
        return buyerMobile;
    }

    public void setBuyerMobile(String buyerMobile) {
        this.buyerMobile = buyerMobile;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getFilterStatus() {
        return filterStatus;
    }

    public void setFilterStatus(Integer filterStatus) {
        this.filterStatus = filterStatus;
    }

    public Long[] getBuyTime() {
        return buyTime;
    }

    public void setBuyTime(Long[] buyTime) {
        this.buyTime = buyTime;
    }

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

    public String getSortType() {
        return sortType;
    }

    public void setSortType(String sortType) {
        this.sortType = sortType;
    }

    public Integer getIsPickUp() {
        return isPickUp;
    }

    public void setIsPickUp(Integer isPickUp) {
        this.isPickUp = isPickUp;
    }
}
