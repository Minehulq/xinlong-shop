package com.xinlong.shop.core.promotion.mapper;

import com.xinlong.shop.core.promotion.entity.PutSaleOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
/**
 * <p>
 * 上架订单列表 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2023-03-29
 */
@Mapper
public interface PutSaleOrderMapper extends BaseMapper<PutSaleOrder> {

}
