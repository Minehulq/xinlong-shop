package com.xinlong.shop.core.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.core.order.entity.Cart;
import com.xinlong.shop.core.order.entity.dto.CartDTO;

import java.util.List;

/**
 * <p>
 * 购物车表 服务类
 * </p>
 *
 * @author Sylow
 * @since 2023-04-21
 */
public interface ICartService extends IService<Cart> {

    void update(Cart cart, Integer id);

    void delete(Integer id);

    void addCart(Cart cart);

    List<CartDTO> findListByMemberId(Integer memberId);

    /**
     * 更新购物车数据
     * @param id
     * @param memberId 会员id
     * @param num 数量
     */
    void updateNum(Integer id, Integer memberId, Integer num);

    void delete(Integer id, Integer memberId);
}
