package com.xinlong.shop.core.member.entity.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xinlong.shop.core.member.entity.MemberSign;

import java.io.Serializable;

/**
 * @Author Sylow
 * @Description 会员签名dto 一般是后台管理分页用
 * @Date: Created in 21:06 2022/11/6
 */
@TableName(autoResultMap = true)
public class MemberSignListDTO extends MemberSign implements Serializable {

   private String memberName;

   private String nickname;

   private String mobile;

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
