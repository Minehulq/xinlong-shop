package com.xinlong.shop.api.promotion;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.core.promotion.entity.PutSaleOrder;
import com.xinlong.shop.core.promotion.service.IPutSaleOrderService;
import com.xinlong.shop.framework.common.R;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 上架订单列表 前端控制器
 * </p>
 * 2023 0330: 管理界面待对接
 * @author Sylow
 * @since 2023-03-29
 */
@RestController
@Api(tags = "上架订单列表")
@RequestMapping("/promotion/put-sale-order")
public class PutSaleOrderController {

    private final IPutSaleOrderService putSaleOrderService;

    public PutSaleOrderController(IPutSaleOrderService putSaleOrderService) {
        this.putSaleOrderService = putSaleOrderService;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size){
        Page<PutSaleOrder> page = new Page<>(current, size);
        page.addOrder(OrderItem.desc("id"));
        page = this.putSaleOrderService.page(page);
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody PutSaleOrder putSaleOrder ){
        this.putSaleOrderService.save(putSaleOrder);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@Valid @RequestBody PutSaleOrder putSaleOrder, @PathVariable("id") Integer id){
        this.putSaleOrderService.update(putSaleOrder, id);
        return R.success("操作成功",putSaleOrder);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        this.putSaleOrderService.delete(id);
        return R.success("删除成功");
    }

    /**
     * 人工收取上架费,上架
     * @param orderSn
     * @return
     */
    @RequestMapping(value = "/put-sale", method = RequestMethod.PUT)
    public R putSale(String orderSn){
        // 上架结果
        putSaleOrderService.putSaleOrder(orderSn, "");
        return R.success("操作成功");
    }


    /**
     * 批量上架临时用
     */
    /**
     * 人工收取上架费,上架
     * @return
     */
    @RequestMapping(value = "/batch-put-sale", method = RequestMethod.PUT)
    public R batchPutSale(Integer memberId){
        List<PutSaleOrder> list = this.putSaleOrderService.findByMemberId(memberId);
        for(PutSaleOrder putSaleOrder : list){
            // 上架结果
            putSaleOrderService.putSaleOrder(putSaleOrder.getOrderSn(), "");
        }
        return R.success("操作成功");
    }


}
