package com.xinlong.shop.api.common;

import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.framework.core.entity.SysSetting;
import com.xinlong.shop.framework.service.ISysSettingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;


/**
 * @Author Sylow
 * @Description 系统配置
 * @Date: Created in 18:36 2022/7/17
 */
@Validated
@Api(tags = "系统配置管理端API")
@RestController
@RequestMapping("/sys/setting")
public class SysSettingController {

    private final ISysSettingService sysSettingService;

    public SysSettingController(ISysSettingService sysSettingService) {
        this.sysSettingService = sysSettingService;
    }

    @ApiOperation(value = "获取某一个分组系统配置")
    @ApiResponses(@ApiResponse(code = 200, message = "操作成功", response = SysSetting.class))
    @RequestMapping(value = "/get", method = RequestMethod.GET, produces = "application/json")
    public R get(@NotBlank(message = "配置名称不能为空") String settingName){
        SysSetting sysSetting = this.sysSettingService.getSetting(settingName);

        return R.success(sysSetting);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody SysSetting sysSetting ){
        this.sysSettingService.saveSetting(sysSetting);
        return R.success("操作成功");
    }


}
