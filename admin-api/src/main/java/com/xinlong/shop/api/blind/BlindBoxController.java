package com.xinlong.shop.api.blind;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.core.blind.entity.BlindBox;
import com.xinlong.shop.core.blind.entity.BlindBoxGoods;
import com.xinlong.shop.core.blind.entity.vo.BlindBoxVO;
import com.xinlong.shop.core.blind.mapstruct.BlindBoxStruct;
import com.xinlong.shop.core.blind.service.IBlindBoxGoodsService;
import com.xinlong.shop.core.blind.service.IBlindBoxService;
import com.xinlong.shop.framework.common.R;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 盲盒 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2023-05-15
 */
@RestController
@RequestMapping("/blind-box/blind-box")
public class BlindBoxController {

    private final IBlindBoxService blindBoxService;
    private final IBlindBoxGoodsService blindBoxGoodsService;
    private final BlindBoxStruct blindBoxStruct;

    public BlindBoxController(IBlindBoxService blindBoxService, IBlindBoxGoodsService blindBoxGoodsService, BlindBoxStruct blindBoxStruct) {
        this.blindBoxService = blindBoxService;
        this.blindBoxGoodsService = blindBoxGoodsService;
        this.blindBoxStruct = blindBoxStruct;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size){
        Page<BlindBox> page = new Page<>(current, size);
        page = this.blindBoxService.page(page);
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody BlindBoxVO blindBoxVO){
        this.blindBoxService.saveBlindBox(blindBoxVO);
        return R.success("操作成功", blindBoxVO);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@Valid @RequestBody BlindBoxVO blindBoxVO, @PathVariable("id") Integer id){
        this.blindBoxService.updateBlindBox(blindBoxVO, id);
        return R.success("操作成功",blindBoxVO);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        this.blindBoxService.delete(id);
        return R.success("删除成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public R get(@PathVariable("id") Integer id){
        BlindBox blindBox = this.blindBoxService.getById(id);
        BlindBoxVO blindBoxVO = blindBoxStruct.toBlindBoxVO(blindBox);
        if (blindBox == null) {
            return R.error("数据不存在");
        }

        List<BlindBoxGoods> goodsList = this.blindBoxGoodsService.findByBlindBoxId(id);
        blindBoxVO.setGoodsList(goodsList);
        return R.success("操作成功", blindBoxVO);
    }

    @RequestMapping(value = "/status", method = RequestMethod.PUT)
    public R updateStatus(Integer id, Integer status){
        this.blindBoxService.updateStatus(id, status);
        return R.success("操作成功");
    }

    /**
     * 获取所有盲盒  给后端选择器使用，分页后期优化
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public R list(String blindBoxName){
        List<BlindBox> list = this.blindBoxService.findAllBlindBox(blindBoxName);
        return R.success(list);
    }

}
