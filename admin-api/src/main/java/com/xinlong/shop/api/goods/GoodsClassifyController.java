package com.xinlong.shop.api.goods;

import com.xinlong.shop.core.goods.entity.vo.GoodsClassifyVO;
import com.xinlong.shop.core.goods.mapstruct.GoodsClassifyStruct;
import org.springframework.web.bind.annotation.*;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.core.goods.entity.GoodsClassify;
import com.xinlong.shop.core.goods.service.IGoodsClassifyService;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2022-11-04
 */
@RestController
@RequestMapping("/goods/classify")
public class GoodsClassifyController {

    private final IGoodsClassifyService goodsClassifyService;

    private final GoodsClassifyStruct goodsClassifyStruct;

    public GoodsClassifyController(IGoodsClassifyService goodsClassifyService, GoodsClassifyStruct goodsClassifyStruct) {
        this.goodsClassifyService = goodsClassifyService;
        this.goodsClassifyStruct = goodsClassifyStruct;
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public R list(Integer pid){
        if (pid == null) {
            pid = 0;
        }
        List<GoodsClassify> list = this.goodsClassifyService.findByPid(pid);
        List<GoodsClassifyVO> voList = goodsClassifyStruct.toGoodsClassifyVO(list);
        return R.success("操作成功", voList);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R add(@RequestBody GoodsClassify goodsClassify ){
        goodsClassify.setId(null);
        this.goodsClassifyService.add(goodsClassify);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@RequestBody GoodsClassify goodsClassify, @PathVariable("id") Integer id){
        this.goodsClassifyService.update(goodsClassify, id);
        return R.success("操作成功",goodsClassify);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        this.goodsClassifyService.delete(id);
        return R.success("删除成功");
    }

    @RequestMapping(value = "/show", method = RequestMethod.PUT)
    public R show(Integer id, Integer isShow) {
        if (isShow == null) {
            isShow = 0;
        }
        this.goodsClassifyService.updateShow(id, isShow);
        return R.success("操作成功");
    }

}
