//package com.xinlong.shop.api.common;
//
//import com.xinlong.shop.framework.common.R;
//import com.xinlong.shop.generator.model.GeneratorVO;
//import com.xinlong.shop.generator.service.IGeneratorService;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.validation.annotation.Validated;
//import org.springframework.web.bind.annotation.*;
//
///**
// * @Author Sylow
// * @Description
// * @Date: Created in 22:55 2022/5/17
// */
//
//@RequestMapping("/sys/gen")
//@RestController
//public class GenController {
//
//    @ApiOperation(value = "代码生成器测试", notes = "代码生成器测试")
//    @RequestMapping(value = "/test-auth1", method = RequestMethod.POST)
//    public R testAuth1(@RequestBody @Validated GeneratorVO generatorVO){
//        return R.success("权限1成功");
//    }
//}
