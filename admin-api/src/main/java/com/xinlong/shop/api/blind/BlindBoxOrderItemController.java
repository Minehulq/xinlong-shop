package com.xinlong.shop.api.blind;

import com.xinlong.shop.core.blind.entity.BlindBoxOrderItem;
import com.xinlong.shop.core.blind.service.IBlindBoxOrderItemService;
import com.xinlong.shop.framework.common.R;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 盲盒订单项 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2023-06-27
 */
@RestController
@Api(tags = "盲盒订单项API")
@RequestMapping("/blind-box/blind-box-order/item")
public class BlindBoxOrderItemController {

    private final IBlindBoxOrderItemService blindBoxOrderItemService;

    public BlindBoxOrderItemController(IBlindBoxOrderItemService blindBoxOrderItemService) {
        this.blindBoxOrderItemService = blindBoxOrderItemService;
    }

    @ApiOperation(value = "获取盲盒订单项列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "blind-box-order-id", value = "订单id", required = true, paramType = "path"),
    })
    @ApiResponses(@ApiResponse(code = 200, message = "操作成功", response = BlindBoxOrderItem.class))
    @RequestMapping(value = "/list/{blind-box-order-id}", method = RequestMethod.GET,  produces = "application/json")
    public R list(@PathVariable("blind-box-order-id") Integer blindBoxOrderId){
        return R.success( this.blindBoxOrderItemService.findByOrderId(blindBoxOrderId));
    }

//    @RequestMapping(value = "/page", method = RequestMethod.GET)
//    public R page(Integer current, Integer size){
//        Page<BlindBoxOrderItem> page = new Page<>(current, size);
//        page = this.blindBoxOrderItemService.page(page);
//        return R.success("操作成功", page);
//    }
//
//    @RequestMapping(value = "", method = RequestMethod.POST)
//    public R save(@Valid @RequestBody BlindBoxOrderItem blindBoxOrderItem ){
//        this.blindBoxOrderItemService.save(blindBoxOrderItem);
//        return R.success("操作成功");
//    }
//
//    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
//    public R update(@Valid @RequestBody BlindBoxOrderItem blindBoxOrderItem, @PathVariable("id") Integer id){
//        this.blindBoxOrderItemService.update(blindBoxOrderItem, id);
//        return R.success("操作成功",blindBoxOrderItem);
//    }
//
//    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
//    public R delete(@PathVariable("id") Integer id) {
//        this.blindBoxOrderItemService.delete(id);
//        return R.success("删除成功");
//    }

}
