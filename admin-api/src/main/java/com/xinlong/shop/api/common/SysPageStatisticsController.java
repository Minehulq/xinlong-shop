package com.xinlong.shop.api.common;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.*;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.framework.core.entity.SysPageStatistics;
import com.xinlong.shop.framework.service.ISysPageStatisticsService;
import javax.validation.Valid;

/**
 * <p>
 * 页面统计 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2023-05-10
 */
@RestController
@RequestMapping("/sys/page-statistics")
public class SysPageStatisticsController {

    private final ISysPageStatisticsService sysPageStatisticsService;

    public SysPageStatisticsController(ISysPageStatisticsService sysPageStatisticsService) {
        this.sysPageStatisticsService = sysPageStatisticsService;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size){
        Page<SysPageStatistics> page = new Page<>(current, size);
        page = this.sysPageStatisticsService.page(page);
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody SysPageStatistics sysPageStatistics ){
        this.sysPageStatisticsService.save(sysPageStatistics);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@Valid @RequestBody SysPageStatistics sysPageStatistics, @PathVariable("id") Integer id){
        this.sysPageStatisticsService.update(sysPageStatistics, id);
        return R.success("操作成功",sysPageStatistics);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        this.sysPageStatisticsService.delete(id);
        return R.success("删除成功");
    }

}
