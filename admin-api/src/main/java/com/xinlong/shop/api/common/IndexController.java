package com.xinlong.shop.api.common;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 00:21 2022/5/17
 */
@RestController
@RequestMapping("/")
public class IndexController {

    @GetMapping("/index")
    public String index() {
        return "index";
    }
}
