package com.xinlong.shop.api.common;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.framework.core.entity.SysAdmin;
import com.xinlong.shop.framework.service.ISysAdminService;
import org.springframework.web.bind.annotation.*;
import com.xinlong.shop.framework.common.R;

import javax.validation.Valid;

/**
 * <p>
 * 系统用户 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2022-10-06
 */
@RestController
@RequestMapping("/sys/admin")
public class SysAdminController {

    private final ISysAdminService sysAdminService;

    public SysAdminController(ISysAdminService sysAdminService) {
        this.sysAdminService = sysAdminService;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size){
        Page<SysAdmin> page = new Page<>(current, size);
        page = this.sysAdminService.page(page);
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody SysAdmin sysAdmin){
        if (StrUtil.isBlank(sysAdmin.getPassword())) {
            return R.error("密码不能为空");
        }
        this.sysAdminService.save(sysAdmin);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@Valid @RequestBody SysAdmin sysAdmin, @PathVariable("id") Integer id){
        this.sysAdminService.update(sysAdmin, id);
        return R.success("操作成功", sysAdmin);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        this.sysAdminService.delete(id);
        return R.success("删除成功");
    }

}
