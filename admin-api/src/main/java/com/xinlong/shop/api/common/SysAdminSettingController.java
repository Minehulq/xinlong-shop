package com.xinlong.shop.api.common;

import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.framework.core.entity.SysAdmin;
import com.xinlong.shop.framework.service.ISysAdminService;
import org.springframework.web.bind.annotation.*;


/**
 * @Author Sylow
 * @Description 系统用户配置
 * @Date: Created in 08:40 2022/9/5
 */
@RestController
@RequestMapping("/sys/admin/setting")
public class SysAdminSettingController {

    private final ISysAdminService sysAdminService;

    public SysAdminSettingController(ISysAdminService sysAdminService) {
        this.sysAdminService = sysAdminService;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@RequestBody SysAdmin sysAdmin, @PathVariable("id") Integer id){
        sysAdmin.setId(id);
        sysAdminService.setUserInfo(sysAdmin);
        return R.success("操作成功", sysAdmin);
    }
}
