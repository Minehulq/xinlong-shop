package com.xinlong.shop.api.site;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinlong.shop.core.site.entity.AdvPromotion;
import com.xinlong.shop.core.site.service.IAdvPromotionService;
import com.xinlong.shop.framework.common.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 广告位 前端控制器
 * </p>
 *
 * @author Sylow
 * @since 2023-05-26
 */
@RestController
@Api(tags = "广告位管理")
@RequestMapping("/site/promotion")
public class AdvPromotionController {

    private final IAdvPromotionService advPromotionService;

    public AdvPromotionController(IAdvPromotionService advPromotionService) {
        this.advPromotionService = advPromotionService;
    }

    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public R page(Integer current, Integer size){
        Page<AdvPromotion> page = new Page<>(current, size);
        page = this.advPromotionService.page(page);
        return R.success("操作成功", page);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public R save(@Valid @RequestBody AdvPromotion advPromotion ){
        advPromotion.setCreateTime(DateUtil.currentSeconds());
        this.advPromotionService.save(advPromotion);
        return R.success("操作成功");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public R update(@Valid @RequestBody AdvPromotion advPromotion, @PathVariable("id") Integer id){
        this.advPromotionService.update(advPromotion, id);
        return R.success("操作成功",advPromotion);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public R delete(@PathVariable("id") Integer id) {
        this.advPromotionService.delete(id);
        return R.success("删除成功");
    }

    @ApiOperation(value = "获取所有广告位")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public R list(){
        List<AdvPromotion> list = this.advPromotionService.list();
        return R.success("操作成功", list);
    }

}
