//package com.xinlong.shop;
//
//import org.mybatis.spring.annotation.MapperScan;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
//import org.springframework.cache.annotation.EnableCaching;
//import springfox.documentation.oas.annotations.EnableOpenApi;
//
///**
// * @Author Sylow
// * @Description
// * @Date: Created in 23:55 2022/5/13
// */
//@EnableOpenApi
//@SpringBootApplication(exclude = {JacksonAutoConfiguration.class})  //解除jackson依赖
//@MapperScan(basePackages = {"com.xinlong.shop.**.mapper"})
//@EnableCaching()
//public class AdminApiApplication {
//
//    public static void main(String[] args) {
//        SpringApplication application = new SpringApplication(AdminApiApplication.class);
//        application.run(args);
//    }
//
//}

// 需要多API就解除掉注释，但是需要换redis或者ehcache做处理，否则会有多套缓存的问题
