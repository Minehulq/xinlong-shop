package com.xinlong.shop;

import cn.hutool.core.io.FileUtil;
import com.google.gson.Gson;
import com.xinlong.shop.core.goods.entity.GoodsClassify;
import com.xinlong.shop.core.goods.service.impl.GoodsClassifyServiceImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 14:57 2022/12/7
 */
@RestController
@RequestMapping("/buyer/classify/test")
public class TestController {

    private final GoodsClassifyServiceImpl goodsClassifyService;

    public TestController(GoodsClassifyServiceImpl goodsClassifyService) {
        this.goodsClassifyService = goodsClassifyService;
    }

    @GetMapping("")
    public String index() {
        String path = "/Users/Sylow/Downloads/classifyJson.txt";
        String classify = FileUtil.readString(path,"utf-8");
        Gson gson = new Gson();
        Map map = new HashMap<String,Object>();
        map = gson.fromJson(classify, Map.class);
        List<Map> lists = (List<Map>) map.get("result");
        addClassify(lists,0);

        return "ok";
    }

    private void addClassify(List<Map> classifys, Integer pid) {
        for(Map classifyMap : classifys) {
            String goodsClassifyName = classifyMap.get("name").toString();
            String goodsClassifyImg = classifyMap.get("image").toString();
            //String parId = classifyMap.get("parentId").toString();

            GoodsClassify goodsClassify = new GoodsClassify();
            goodsClassify.setClassifyName(goodsClassifyName);
            goodsClassify.setClassifyImg(goodsClassifyImg);
            goodsClassify.setPid(pid);
            this.goodsClassifyService.add(goodsClassify);
            Integer parId = goodsClassify.getId();
            System.out.println(classifyMap.get("children"));
            if (classifyMap.get("children") != null) {
                List<Map> childrens = (List<Map>) classifyMap.get("children");
                addClassify(childrens,parId);
            }
        }
    }
}
