package com.xinlong.shop.security;


import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Web 安全配置
 * @Author Sylow
 * @Date 2022/5/21
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Order(101)
public class AdminSecurityConfig extends WebSecurityConfigurerAdapter {


    /**
     * 此方法配置的资源路径不会进入 Spring Security 机制进行验证
     */
    @Override
    public void configure(WebSecurity web) {
        web.ignoring()
                .antMatchers(HttpMethod.OPTIONS, "/**")
                .antMatchers("/favicon.ico")
                .antMatchers("/swagger-ui/**")
                .antMatchers("/v3/api-docs")
                .antMatchers("/webjars/**")
                .antMatchers("/swagger-resources/**")
                .antMatchers("/sys/admin/login")
                .antMatchers("/sys/admin/token")

                // 系统配置
                .antMatchers("/sys/setting/get")

                .antMatchers("/logout")
                .antMatchers("/doc.html");
    }

    /**
     * 添加管理端的配置，单机启动用得到，后期换成redis、多个springboot工程时，就不需要
     * @param web
     */
    public WebSecurity overrideConfig(WebSecurity web) {
        web.ignoring()
                .antMatchers(HttpMethod.OPTIONS, "/**")
                .antMatchers("/favicon.ico")
                .antMatchers("/swagger-ui/**")
                .antMatchers("/v3/api-docs")
                .antMatchers("/webjars/**")
                .antMatchers("/swagger-resources/**")
                .antMatchers("/sys/admin/login")
                .antMatchers("/sys/admin/token")

                .antMatchers("/logout")
                .antMatchers("/doc.html");
        return web;
    }

}
