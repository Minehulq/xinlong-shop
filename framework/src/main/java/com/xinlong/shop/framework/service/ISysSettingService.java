package com.xinlong.shop.framework.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.framework.core.entity.SysSetting;

import java.util.Map;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 10:24 2023/3/29
 */
public interface ISysSettingService extends IService<SysSetting> {

    Map<String, Object> getSetting();

    SysSetting getSetting(String settingName);

    boolean saveSetting(SysSetting sysSetting);

}
