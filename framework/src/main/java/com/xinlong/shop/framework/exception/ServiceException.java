package com.xinlong.shop.framework.exception;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 02:50 2022/5/21
 */
public class ServiceException extends RuntimeException {
    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
