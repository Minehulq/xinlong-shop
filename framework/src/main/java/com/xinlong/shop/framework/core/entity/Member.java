package com.xinlong.shop.framework.core.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 会员
 * </p>
 *
 * @author Sylow
 * @since 2022-11-04
 */
@TableName("s_member")
public class Member implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 会员名称
     */
    private String memberName;

    /**
     * 会员编号
     */
    private String memberNo;

    /**
     * 会员密码
     */
    private String password;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 性别,1=男,0=女
     */
    private Integer sex;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 头像地址
     */
    private String face;

    /**
     * 余额
     */
    private BigDecimal balance;

    /**
     * 用户积分
     */
    private BigDecimal points;

    /**
     * 状态:0=正常,1=禁用
     */
    private Integer status;

    /**
     * 会员等级id
     */
    private Integer gradeId;

    /**
     * 邀请人会员id
     */
    private Integer inviteMemberId;

    /**
     * 微信openId
     */
    private String openId;

    /**
     * 创建时间(注册时间)
     */
    private Long createTime;

    /**
     * 上次登录时间
     */
    private Long lastLogin;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }
    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getPoints() {
        return points;
    }

    public void setPoints(BigDecimal points) {
        this.points = points;
    }

    public String getFace() {
        return face;
    }

    public void setFace(String face) {
        this.face = face;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getGradeId() {
        return gradeId;
    }

    public void setGradeId(Integer gradeId) {
        this.gradeId = gradeId;
    }

    public Integer getInviteMemberId() {
        return inviteMemberId;
    }

    public void setInviteMemberId(Integer inviteMemberId) {
        this.inviteMemberId = inviteMemberId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }
    public Long getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Long lastLogin) {
        this.lastLogin = lastLogin;
    }

    @Override
    public String toString() {
        return "Member{" +
            "id=" + id +
            ", memberName=" + memberName +
            ", memberNo=" + memberNo +
            ", password=" + password +
            ", mobile=" + mobile +
            ", sex=" + sex +
            ", nickname=" + nickname +
            ", face=" + face +
            ", status=" + status +
            ", inviteMemberId=" +inviteMemberId +
            ", gradeId=" + gradeId +
            ", openId=" + openId +
            ", createTime=" + createTime +
            ", lastLogin=" + lastLogin +
        "}";
    }
}
