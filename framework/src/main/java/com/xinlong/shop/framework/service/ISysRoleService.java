package com.xinlong.shop.framework.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xinlong.shop.framework.core.entity.SysRole;

import java.util.List;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 21:53 2022/5/31
 */
public interface ISysRoleService extends IService<SysRole> {

    List<SysRole> findAllRoles();

    List<SysRole> findRolesByAdminId(Integer adminId);

    List<SysRole> findRolesByPermission(String permission);

    void update(SysRole sysRole, Integer id);

    void addSysRole(SysRole sysRole);

    void delete(Integer id);

    /**
     * 更新权限
     * @param id
     * @param permissions
     */
    void updatePermission(Integer id, String permissions);

}
