package com.xinlong.shop.framework.cache;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 19:33 2022/5/20
 */
public interface ICache<T> {


    /**
     * 增加
     * @param key
     * @param value
     */
    void put(String key, Object value);

    /**
     * 增加
     * @param key
     * @param value
     * @param expirationTime 过期时间
     */
    void put(String key, Object value, int expirationTime);

    /**
     * 修改
     * @param key
     * @param value
     */
    void modify(String key, Object value);

    /**
     * 获取
     * @param key
     * @return
     */
    T get(String key);


    /**
     * 删除
     * @param key
     */
    void delete(String key);

}
