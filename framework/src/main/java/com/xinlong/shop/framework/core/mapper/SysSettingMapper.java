package com.xinlong.shop.framework.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xinlong.shop.framework.core.entity.SysSetting;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author Sylow
 * @Description 系统配置mapper
 * @Date: Created in 15:59 2023/6/19
 */
@Mapper
public interface SysSettingMapper extends BaseMapper<SysSetting> {


}
