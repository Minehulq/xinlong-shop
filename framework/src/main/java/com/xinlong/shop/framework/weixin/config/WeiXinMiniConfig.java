package com.xinlong.shop.framework.weixin.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 05:08 2023/8/7
 */
@Component
public class WeiXinMiniConfig {

    @Value("${xinlong.weixin.mini.appid}")
    private String appid;

    @Value("${xinlong.weixin.mini.app-secret}")
    private String appSecret;

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }
}
