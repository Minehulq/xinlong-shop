package com.xinlong.shop.framework.core.mapper;

import com.xinlong.shop.framework.core.entity.Member;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.mapstruct.Mapper;

/**
 * <p>
 * 会员 Mapper 接口
 * </p>
 *
 * @author Sylow
 * @since 2022-11-04
 */
@Mapper
public interface MemberMapper extends BaseMapper<Member> {

    Member selectByMemberNo(@Param("memberNo") String memberNo);

}
