package com.xinlong.shop.framework.weixin.model;

/**
 * 微信支付通知回调
 * @Author Sylow
 * @Description
 * @Date: Created in 22:54 2023/3/22
 */
public class WeiXinNotify {
    private String id;
    private String createTime;
    private String resourceType;
    private String eventType;
    private String summary;
    private WeiXinResource resource;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public WeiXinResource getResource() {
        return resource;
    }

    public void setResource(WeiXinResource resource) {
        this.resource = resource;
    }
}
