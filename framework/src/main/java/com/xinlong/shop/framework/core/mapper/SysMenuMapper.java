package com.xinlong.shop.framework.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xinlong.shop.framework.core.entity.SysMenu;
import com.xinlong.shop.framework.core.model.SysMenuDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 21:45 2022/5/31
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    List<SysMenu> findAllMenus();

    List<SysMenuDTO> findMenusForDTO();
}
