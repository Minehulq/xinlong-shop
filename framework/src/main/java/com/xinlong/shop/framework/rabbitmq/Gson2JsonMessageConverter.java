//package com.xinlong.shop.framework.rabbitmq;
//
//import com.google.gson.Gson;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.amqp.core.Message;
//import org.springframework.amqp.core.MessageDeliveryMode;
//import org.springframework.amqp.core.MessageProperties;
//import org.springframework.amqp.support.converter.ClassMapper;
//import org.springframework.amqp.support.converter.DefaultClassMapper;
//import org.springframework.amqp.support.converter.MessageConversionException;
//import org.springframework.amqp.support.converter.MessageConverter;
//
//import java.io.IOException;
//import java.io.UnsupportedEncodingException;
//
//
///**
// * @Author Sylow
// * @Description 自定义MessageConverter 使用gson
// * @Date: Created in 23:07 2023/8/17
// */
//public class Gson2JsonMessageConverter implements MessageConverter {
//
//    private static final Logger logger = LoggerFactory.getLogger(Gson2JsonMessageConverter.class);
//
//    private static Gson gson = new Gson();
//    private final static String CHARSET_NAME = "UTF-8";
//
//    private static ClassMapper classMapper =  new DefaultClassMapper();
//
//
//    @Override
//    public Message toMessage(Object o, MessageProperties messageProperties) throws MessageConversionException {
//        //约定，发送消息的除了Bean 就是 String, 不会有其他类型
//        byte[] bytes = null;
//        try {
//            String jsonString = null;
//            if(o instanceof String){
//                jsonString=(String) o;
//            }else {
//                jsonString = gson.toJson(o);
//            }
//            bytes = jsonString.getBytes(CHARSET_NAME);
//        }
//        catch (IOException e) {
//            throw new MessageConversionException(
//                    "Failed to convert Message content", e);
//        }
//        messageProperties.setContentType(MessageProperties.CONTENT_TYPE_JSON);
//        messageProperties.setContentEncoding(CHARSET_NAME);
//        //设置消息持久化
//        messageProperties.setDeliveryMode(MessageDeliveryMode.PERSISTENT);
//        if (bytes != null) {
//            messageProperties.setContentLength(bytes.length);
//        }
//        classMapper.fromClass(o.getClass(),messageProperties);
//        return new Message(bytes, messageProperties);
//    }
//
//    @Override
//    public Object fromMessage(Message message) throws MessageConversionException {
//        Object content = null;
//        MessageProperties properties = message.getMessageProperties();
//        if (properties != null) {
//            String contentType = properties.getContentType();
//            if (contentType != null && contentType.contains("json")) {
//                String encoding = properties.getContentEncoding();
//                if (encoding == null) {
//                    encoding = CHARSET_NAME;
//                }
//                try {
//                    Class<?> targetClass = classMapper.toClass(properties);
//                    content = convertBytesToObject(message.getBody(),
//                            encoding, targetClass);
//                }
//                catch (IOException e) {
//                    throw new MessageConversionException(
//                            "Failed to convert Message content", e);
//                }
//            }
//            else {
//                logger.warn("Could not convert incoming message with content-type ["
//                        + contentType + "]");
//            }
//        }
//        if (content == null) {
//            content = message.getBody();
//        }
//        return content;
//    }
//
//    private Object convertBytesToObject(byte[] body, String encoding,
//                                        Class<?> clazz) throws UnsupportedEncodingException {
//        String contentAsString = new String(body, encoding);
//        return gson.fromJson(contentAsString, clazz);
//    }
//}
