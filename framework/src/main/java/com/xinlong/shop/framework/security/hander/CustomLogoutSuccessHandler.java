package com.xinlong.shop.framework.security.hander;

import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import com.xinlong.shop.framework.common.R;
import com.xinlong.shop.framework.security.constant.HttpStatusConstants;
import com.xinlong.shop.framework.util.JsonUtil;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义退出成功
 * @Author Sylow
 * @Date 2022/5/27
 */
@Component
public class CustomLogoutSuccessHandler implements LogoutSuccessHandler {



    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");

        Integer code = HttpStatusConstants.SUCCESS;
        String msg ="退出成功";

        response.getWriter().println(JsonUtil.objectToJson(R.error(code, msg)));
        response.getWriter().flush();

    }

}
