package com.xinlong.shop.framework.sms;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 00:37 2023/1/1
 */
@Component
public class AliyunSMSConfig {

    @Value("${xinlong.aliyun.sms.signName}")
    private String signName;

    @Value("${xinlong.aliyun.sms.regionId}")
    private String regionId;

    @Value("${xinlong.aliyun.sms.templateCode}")
    private String templateCode;

    @Value("${xinlong.aliyun.sms.accessKeyId}")
    private String accessKeyId;

    @Value("${xinlong.aliyun.sms.access-key-secret}")
    private String accessKeySecret;

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getTemplateCode() {
        return templateCode;
    }

    public void setTemplateCode(String templateCode) {
        this.templateCode = templateCode;
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getAccessKeySecret() {
        return accessKeySecret;
    }

    public void setAccessKeySecret(String accessKeySecret) {
        this.accessKeySecret = accessKeySecret;
    }
}
