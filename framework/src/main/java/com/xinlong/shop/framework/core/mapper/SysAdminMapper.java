package com.xinlong.shop.framework.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xinlong.shop.framework.core.entity.SysAdmin;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 01:24 2022/5/25
 */
@Mapper
public interface SysAdminMapper extends BaseMapper<SysAdmin> {

    SysAdmin getByUserName(@Param("userName") String userName);

}
