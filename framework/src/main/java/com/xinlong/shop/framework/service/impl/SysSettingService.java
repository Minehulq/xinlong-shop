package com.xinlong.shop.framework.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xinlong.shop.framework.core.entity.SysSetting;
import com.xinlong.shop.framework.core.mapper.SysSettingMapper;
import com.xinlong.shop.framework.service.ISysSettingService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author Sylow
 * @Description
 * @Date: Created in 10:26 2023/3/29
 */
@Component
public class SysSettingService extends ServiceImpl<SysSettingMapper, SysSetting> implements ISysSettingService {

    // 费用增幅百分比
    @Value("${xinlong.shop.price-markup}")
    private Double priceMarkup;
    // 强制消费积分起点
    @Value("${xinlong.shop.bonus-points}")
    private Double bonusPoints;
    // 服务费百分比
    @Value("${xinlong.shop.put-sale-price}")
    private Double putSalePrice;
    // 服务费内佣金占比
    @Value("${xinlong.shop.put-sale-price-commission}")
    private Double putSalePriceCommission;
    // 微信支付开关
    @Value("${xinlong.shop.weixin-pay}")
    private boolean openWeixinPay;
    // 快递100配置
    @Value("${xinlong.shop.kuaidi100-key}")
    private String kuaidi100Key;
    @Value("${xinlong.shop.kuaidi100-customer}")
    private String kuaidi100Customer;

    @Value("${xinlong.tencent.secret-id}")
    private String tencentSecretId;

    @Value("${xinlong.tencent.secret-key}")
    private String tencentSecretKey;



    /**
     * 后期作废 后期改造进数据库
     * @return
     */
    @Override
    public Map<String, Object> getSetting() {
        // 获取配置 后期改为存储数据库 做成缓存
        Map<String, Object> setting = new HashMap<>();
        setting.put("priceMarkup", priceMarkup);
        setting.put("bonusPoints", bonusPoints);
        setting.put("putSalePrice", putSalePrice);
        setting.put("putSalePriceCommission", putSalePriceCommission);
        setting.put("openWeixinPay", openWeixinPay);
        //
        setting.put("kuaidi100Key", kuaidi100Key);
        setting.put("kuaidi100Customer", kuaidi100Customer);
        //
        setting.put("tencentSecretId", tencentSecretId);
        setting.put("tencentSecretKey", tencentSecretKey);
        return setting;
    }

    @Override
    public SysSetting getSetting(String settingName) {
        QueryWrapper<SysSetting> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("setting_name", settingName);
        SysSetting sysSetting = this.getOne(queryWrapper);
        return sysSetting;
    }

    @Override
    public boolean saveSetting(SysSetting sysSetting) {
        if (sysSetting.getId() != null) {
            return this.saveOrUpdate(sysSetting);
        } else {
            return this.save(sysSetting);
        }
    }

    public Double getPriceMarkup() {
        return priceMarkup;
    }

    public void setPriceMarkup(Double priceMarkup) {
        this.priceMarkup = priceMarkup;
    }

    public Double getPutSalePrice() {
        return putSalePrice;
    }

    public void setPutSalePrice(Double putSalePrice) {
        this.putSalePrice = putSalePrice;
    }

    public Double getPutSalePriceCommission() {
        return putSalePriceCommission;
    }

    public void setPutSalePriceCommission(Double putSalePriceCommission) {
        this.putSalePriceCommission = putSalePriceCommission;
    }

    public boolean isOpenWeixinPay() {
        return openWeixinPay;
    }

    public void setOpenWeixinPay(boolean openWeixinPay) {
        this.openWeixinPay = openWeixinPay;
    }
}
