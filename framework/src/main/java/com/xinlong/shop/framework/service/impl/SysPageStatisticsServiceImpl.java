package com.xinlong.shop.framework.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xinlong.shop.framework.core.entity.SysPageStatistics;
import com.xinlong.shop.framework.core.mapper.SysPageStatisticsMapper;
import com.xinlong.shop.framework.service.ISysPageStatisticsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 页面统计 服务实现类
 * </p>
 *
 * @author Sylow
 * @since 2023-05-10
 */
@Service
public class SysPageStatisticsServiceImpl extends ServiceImpl<SysPageStatisticsMapper, SysPageStatistics> implements ISysPageStatisticsService {

    @Override
    public void update(SysPageStatistics sysPageStatistics, Integer id) {
        UpdateWrapper<SysPageStatistics> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        this.update(sysPageStatistics, updateWrapper);
    }
    @Override
    public void delete(Integer id) {
        UpdateWrapper<SysPageStatistics> deleteWrapper = new UpdateWrapper<>();
        deleteWrapper.eq("id", id);
        this.remove(deleteWrapper);
    }

    @Override
    public int visitNum(Integer rushPurchaseId, String pageId) {
        return this.baseMapper.rushPurchaseVisitNum(rushPurchaseId, pageId);
    }


}
