package com.xinlong.shop.framework.weixin.model;

/**
 * 微信回调通知对象Resource
 * @Author Sylow
 * @Description
 * @Date: Created in 22:55 2023/3/22
 */
public class WeiXinResource {
    private String originalType;
    private String algorithm;
    private String ciphertext;
    private String associatedData;
    private String nonce;

    public String getOriginalType() {
        return originalType;
    }

    public void setOriginalType(String originalType) {
        this.originalType = originalType;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public String getCiphertext() {
        return ciphertext;
    }

    public void setCiphertext(String ciphertext) {
        this.ciphertext = ciphertext;
    }

    public String getAssociatedData() {
        return associatedData;
    }

    public void setAssociatedData(String associatedData) {
        this.associatedData = associatedData;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }
}
